package vlc;

import java.lang.reflect.Array;
import java.util.*;

public class Singleton {

	private static Singleton adminobject = new Singleton();	
// All admin users will be stored in the following map.
	private Map<String, LoginData> Login_list = new HashMap<> (); 
	private ArrayList<AppointmentData> appointment_list = new ArrayList (); 
	private ArrayList<ClassbookingData> Classbooking_list = new ArrayList ();
	private ArrayList<FeebillingData> Feebillingdata_list = new ArrayList ();
	private ArrayList<StudentregistrationData> Studentregistrationdata_list = new ArrayList ();
	private ArrayList<CoachregistrationData> Coachregistrationdata_list = new ArrayList ();
	private ArrayList<AttendanceData> Attendance_list = new ArrayList ();
	
	private Singleton(){}
	public static Singleton getInstance(){
		return adminobject;
	}
	public Map<String, LoginData> getAdminUserList(){
		return Login_list;
	}
	public ArrayList<AppointmentData> getappointmentdataList(){
		return appointment_list;
	}
	public ArrayList<ClassbookingData> getclassbookingdataList(){
		return Classbooking_list;
	}
	public ArrayList<FeebillingData> getfeebillingdataList(){
		return Feebillingdata_list;
	}
	public ArrayList<StudentregistrationData> getstudentregistrationdataList(){
		return Studentregistrationdata_list;
	}
	public ArrayList<CoachregistrationData> getcoachregistrationdataList(){
		return Coachregistrationdata_list;
	}
	
	public ArrayList<AttendanceData> getattendancedataList(){
		return Attendance_list;
	}

}


