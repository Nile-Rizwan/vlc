package vlc;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import com.toedter.calendar.JDateChooser;

import lu.tudor.santec.jtimechooser.JTimeChooser;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.*;
import java.util.ArrayList;


public class StudentAttendance {
	 JFrame frame; 
	 JMenuBar menubar7;
	 JMenu filemenu;  JMenuItem Exit;
	 JMenu openmenu;  JMenuItem studentregistration_mi;  JMenuItem coachregistration_mi;  JMenuItem records_mi;  JMenuItem appointment_mi;  JMenuItem feebilling_mi; JMenuItem classbokking_mi;
	 JMenu Logoutmenu; JMenuItem Logout_mi;
	 JMenu Helpmenu; JMenuItem Help_mi;
	 JLabel Coach_lookup;
	
	 JLabel Coach_name;
	 JTextField Coach_name_tf;
	 JLabel Student_name;
	 JTextField Student_name_tf;
	 JLabel Class;              JComboBox Class_cb;
	 JLabel Room;               JComboBox Room_cb;
	 JLabel Date;               JDateChooser date_dc;
	 JLabel Time;               JTimeChooser Time_tc;      JComboBox Time_cb;
	 JLabel Attendance;
	 JRadioButton Present_rbtn;           JRadioButton Absent_rbtn;           JRadioButton Leave_rbtn;             JRadioButton Late_rbtn;
	 ButtonGroup Attendance_bg;
	 JButton Load_data_btn;
	 JTable Attendance_table;
	 JScrollPane Attendance_table_sp;
	 JButton Save_btn;
	 JButton Back_btn;
	 JButton Clear_btn;
	 JButton Exit_btn;
	 JLabel Search;
	 JTextField Search_tf;
	 
	 JLabel Logo;
	 DefaultTableModel model;
	 


	 StudentAttendance() 
	    { 
	         
	        frame=new JFrame("Venue Leisure Centre"); 
	        Image img = Toolkit.getDefaultToolkit().getImage("Resources/img/vlcmin.PNG");
	        frame.setIconImage(img);
	        
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
	        
	        frame.setSize(1235, 441); 
	        
	        frame.setLocationRelativeTo(null);
	          
	        frame.getContentPane().setLayout(null);   
	          
	        frame.setVisible(true);
	        
	        menubar7 = new JMenuBar ();
	        frame.setJMenuBar (menubar7);
	        
	        filemenu = new JMenu ("File");
	        menubar7.add (filemenu);
	        
	       
	        
	        openmenu = new JMenu ("Open");
	        filemenu.add (openmenu);
	        openmenu.setIcon(new ImageIcon(this.getClass().getResource("/icon/open.png")));
	        
	        studentregistration_mi = new JMenuItem("Student Registration");
	        studentregistration_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Studentregistration.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(studentregistration_mi);
	        studentregistration_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/student reg jmi.png"))); 
	        
	        openmenu.add(new JSeparator());
	        
	        coachregistration_mi = new JMenuItem("Coach Registration");
	        coachregistration_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Coachregistration.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(coachregistration_mi);
	        coachregistration_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/coach reg jmi.png"))); 
	        
	        openmenu.add(new JSeparator());
	        
	        records_mi = new JMenuItem("Records");
	        records_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Report.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(records_mi);
	        records_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/records jmi.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        appointment_mi = new JMenuItem("Appointment");
	        appointment_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Appointment.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(appointment_mi);
	        appointment_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/appointment jmi.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        feebilling_mi = new JMenuItem("Fee Billing");
	        feebilling_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Feebilling.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(feebilling_mi);
	        feebilling_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/fee billing jmi.png")));
	        
	        filemenu.add(new JSeparator());
	        
	        Exit = new JMenuItem("Exit");
	        Exit.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
			             int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
			             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

			                    {
			                     System.exit(0);
			                    }
	        	}
	        });
	        filemenu.add(Exit);
	        Exit.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png")));
	        
openmenu.add(new JSeparator());
	        
	        classbokking_mi = new JMenuItem("Class Booking");
	        classbokking_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Classbooking.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(classbokking_mi);
	        classbokking_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/class booking jmi.png")));
	        
	        Logoutmenu = new JMenu ("Logout");
	        menubar7.add (Logoutmenu);
	         
	        
	        Logout_mi = new JMenuItem("Logout");
	        Logout_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
	        		int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to logout?","Logout",JOptionPane.YES_NO_OPTION);
		             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 
		                    {
		            	 Loginselection.main(null);
			                 frame.dispose();
		                    }
	        	}
	        });
	        Logoutmenu.add(Logout_mi);
	        Logout_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/logout.png"))); 
	        
	        Helpmenu = new JMenu ("Help");
	        menubar7.add (Helpmenu);
	         
	        
	        Help_mi = new JMenuItem("Help");
	        Help_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Help.main(null); 
	        	}
	        });
	        
	        Helpmenu.add(Help_mi);
	        Help_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/help.png")));
	        
	        
            Coach_lookup = new JLabel("Attendence"); 
	        
                        Coach_lookup.setBounds(418, 13, 126, 26); 
	          
	                                frame.getContentPane().add(Coach_lookup); 
	                                
	                                Coach_name = new JLabel("Coach Name");
	        	        	        
	                                Coach_name.setBounds(12, 55, 97, 26);
	               	        	        	        
	               	        	       frame.getContentPane().add(Coach_name);
	               	        	                    
	               	        	    Coach_name_tf = new JTextField();              	        	        	        
	               	        	        	        
	               	        	 Coach_name_tf.setBounds(113, 57, 210, 22);
	               	        	
	               	        	frame.getContentPane().add(Coach_name_tf);
	               	        	
	               	        	Student_name = new JLabel("Student Name"); 
	        	   	   	        
	   	                        Student_name.setBounds(12, 94, 97, 26); 
	   	   	          
	   	   	                          frame.getContentPane().add(Student_name);
	   	   	
	   	               Student_name_tf = new JTextField(); 
	   	   	        
	   	                           Student_name_tf.setBounds(113, 96, 210, 22);
	   	   	          
	   	   	                                 frame.getContentPane().add(Student_name_tf);
	       
         
	        Class = new JLabel("Class");
	        	        
	        	 Class.setBounds(12, 135, 97, 26);
	        	        	        
	        	       frame.getContentPane().add(Class);
	        	                    
	        Class_cb = new JComboBox();
	        	        	        
	        	    Class_cb.setModel(new DefaultComboBoxModel(new String[] {"Swimming", "badminton", "gym"}));
	        	        	        
	        	    Class_cb.setBounds(113, 135, 87, 26);
	        	        			
	        	        	 frame.getContentPane().add(Class_cb);
	        	        	 
	        	        	 
	        	        	 Room = new JLabel("Alloted Room");
	        	 	        
	        	        	 Room.setBounds(12, 170, 97, 26);
	 	        
	                                      frame.getContentPane().add(Room);
	             
	                                      Room_cb = new JComboBox();
	 	        
	                                      Room_cb.setModel(new DefaultComboBoxModel(new String[] {"Studio A", "Studio B", "Studio C", "Pool", "Gym"}));
	 	        
	                                      Room_cb.setBounds(113, 172, 87, 28);
	 			
	 	                                       frame.getContentPane().add(Room_cb);
	        	        
	        
	        	        
	        Date = new JLabel("Date"); 
	        	        	        	        
	        	Date.setBounds(20, 207, 87, 26); 
	        	        	        	        	          
	        	     frame.getContentPane().add(Date);
	        	        	        	        	        
	        date_dc = new JDateChooser();
	        
	        	   date_dc.setBounds(113, 213, 200, 20);
	        	   
	        	   
   	        	           frame.getContentPane().add(date_dc);
	        	        	        	              
	        Time = new JLabel("Time"); 
	        	        	        	        	        
	        	Time.setBounds(20, 239, 42, 26); 
	        	        	        	        	        	          
	        	     frame.getContentPane().add(Time);
	        	        	        	        	        	        
	        Time_tc = new JTimeChooser();
	        	   Time_tc.setBounds(113, 243, 118, 22);
	        	          frame.getContentPane().add(Time_tc);
	        Time_cb = new JComboBox();
	        	     	        	        	        	        	        
	        	   Time_cb.setModel(new DefaultComboBoxModel(new String[] {"am", "pm"}));
	        	     	        	        	        	        	        
	        	   Time_cb.setBounds(259, 240, 64, 26);
	        	     	        	        	        	        			
	        	     	   frame.getContentPane().add(Time_cb);
	        	     	   
	        	     	   
	        	     	   
	        	            Attendance = new JLabel("Gender"); 
	        		        
	        	            Attendance.setBounds(12, 276, 51, 26); 
	     	        	          
	     	                      frame.getContentPane().add(Attendance);
	     	        	
	                 Present_rbtn = new JRadioButton("Present");
	                 
	                 Present_rbtn.setActionCommand("Present");

	                 Present_rbtn.setBounds(113, 279, 87, 23);
	     	                    
	     	                          frame.getContentPane().add(Present_rbtn);

	                 Absent_rbtn = new JRadioButton("Absent");
	                 
	                 Absent_rbtn.setActionCommand("Absent");

	                 Absent_rbtn.setBounds(204, 278, 75, 23);
	     	                     
	     	                               frame.getContentPane().add( Absent_rbtn);
	     	                      
	                 Leave_rbtn = new JRadioButton("Leave");
	                 
	                 Leave_rbtn.setActionCommand("Leave");

	                 Leave_rbtn.setBounds(113, 305, 87, 23);
	     	                      
	     	                            frame.getContentPane().add(Leave_rbtn);
	     	                             
	     	                           Late_rbtn = new JRadioButton("Late");
	     	                           
	     	                          Late_rbtn.setActionCommand("Late");

	     	     	                 Late_rbtn.setBounds(204, 304, 63, 23);
	     	     	     	                      
	     	     	     	                            frame.getContentPane().add(Late_rbtn);
	     	        
	     	        Attendance_bg = new ButtonGroup();
	     	       Attendance_bg.add(Present_rbtn);
	     	      Attendance_bg.add(Absent_rbtn);
	     	     Attendance_bg.add(Leave_rbtn);
	        	 Attendance_bg.add(Late_rbtn);  	   
	        	     	   
	        	     	   
	        	     	   
	        	     	   
	        	     	   
	        Load_data_btn = new JButton("Load Data"); 
	        Load_data_btn.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
	        		ArrayList<AttendanceData> vector = Singleton.getInstance().getattendancedataList();
	   		            String[] columnNames = {"Coach Name", "Student Name", "Class", "Room", "Date", "Time", "am/pm", "Attendance"}; 

	   		  	 Object[][] data;
	   		  	 
				data = new Object[vector.size()][9];

				for (int i = 0; i < data.length; i++) {
						AttendanceData a = vector.get(i);
					
					data[i][0] = a.getCoach_name_tf();
					data[i][1] = a.getStudent_name_tf();
					data[i][2] = a.getClass_cb();
					data[i][3] = a.getRoom_cb();
					data[i][4] = a.getDate_dc();
					data[i][5] = a.getTime_tc();
					data[i][6] = a.getTime_cb();
					data[i][7] = a.getAttendance_bg();
					
			
					
					
				}
	   				model = new DefaultTableModel(data, columnNames);
	   				Attendance_table = new JTable(model);
	   				Attendance_table_sp = new JScrollPane(Attendance_table);

	   				Attendance_table_sp.setVisible(true);

	   				Attendance_table_sp.setBounds(347, 55, 650, 295);

	                     frame.getContentPane().add(Attendance_table_sp);	
	                  
	                 }
	                 
              });
	        	
	        
	        Load_data_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/load data.png"))); 
  	                    
	                     Load_data_btn.setBounds(1009, 55, 124, 33);
		        	        	        	                    	   	          
		                               frame.getContentPane().add(Load_data_btn);
		                               
		                               Save_btn = new JButton("Save"); 
		                               Save_btn.addActionListener(new ActionListener() {
		                               	public void actionPerformed(ActionEvent e) {
		                               		ArrayList<AttendanceData> al = Singleton.getInstance().getattendancedataList();
		                               		AttendanceData cbd = new AttendanceData(Coach_name_tf.getText(),Student_name_tf.getText(),Class_cb.getSelectedItem().toString(), Room_cb.getSelectedItem().toString(), date_dc.getDate().toString(), Time_tc.getTimeField().getText(), Time_cb.getSelectedItem().toString(), Attendance_bg.getSelection().getActionCommand());
		   		                	 
       		   		                	 al.add(cbd);
       		   		                	JOptionPane.showMessageDialog(null, "Data has been successfully inserted");        
       		   		                 }
       		   	                });
		                               	
		                               
		                               Save_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/save.png")));
			 	 	   			        
			 	                         Save_btn.setBounds(1009, 90, 124, 33);
			 	     	   			          
			 	     	   	                    frame.getContentPane().add(Save_btn);
	        	     	        	        	        	        	        
    
            Back_btn = new JButton("Back"); 
            Back_btn.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) {
		     Mainpage.main(null);
		     frame.dispose();
	}
});
            Back_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/back.png"))); 
	   	        
            Back_btn.setBounds(1009, 125, 124, 33);
	   	          
	   	             frame.getContentPane().add(Back_btn);
	   	    
            Clear_btn = new JButton("Clear");
	   	    Clear_btn.addActionListener(new ActionListener() {
	   	 	  public void actionPerformed(ActionEvent e) {
	   	 		
	   	 		Coach_name_tf.setText("");
	   	     
	   	      Student_name_tf.setText("");
	   	     
	   	      Class_cb.setSelectedIndex(0);  
	   	      
	   	      Room_cb.setSelectedIndex(0); 
	   	      
	   	      date_dc.setDate(null);
	   	      
	   	      Time_cb.setSelectedIndex(0);
	   	      
	   	      Time_tc.getTimeField().setValue("00:00:00");
	   	      
	   	      Attendance_bg.clearSelection();
	   	 
	   	 	}
	   	 });
	   		   	 
	   	    Clear_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/clear.png")));
	   	    
            Clear_btn.setBounds(1009, 160, 124, 33);
	   		   	          
	   		   	      frame.getContentPane().add(Clear_btn);
	   	        

            Exit_btn = new JButton("Exit"); 
            Exit_btn.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent arg0) {
		     int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
		     if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

		              {
		               System.exit(0);
		              }
	}
});
            Exit_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png"))); 
            
            Exit_btn.setBounds(1009, 195, 124, 33);
	   				          
	   		         frame.getContentPane().add(Exit_btn);
	   		         
	   		      Search = new JLabel("Search"); 
 			        
	   		         Search.setBounds(723, 30, 51, 26); 
		          
		                          frame.getContentPane().add(Search);
		
		                          Search_tf = new JTextField(); 
		                          Search_tf.addKeyListener(new KeyAdapter() {
		                          	@Override
		                          	public void keyReleased(KeyEvent e) {
		                          		 DefaultTableModel model = (DefaultTableModel)Attendance_table.getModel();
		                                 String search = Student_name_tf.getText();
		                                 TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(model);
		                                 Attendance_table.setRowSorter(tr);
		                                 tr.setRowFilter(RowFilter.regexFilter(search));	
		                          	}
		                          });
		                          Search_tf.setBounds(786, 32, 210, 22);
		        		          
	                                frame.getContentPane().add(Search_tf);
	   		
            //Class_booking_table = new JTable();
	   		
            //Class_booking_table.setBounds(385, 55, 639, 295);
			
                       //frame.getContentPane().add(Class_booking_table);
                       
            //Class_booking_table_sp = new JScrollPane(Class_booking_table);
   	       	        
                                  //Class_booking_table_sp.setBounds(347, 55, 639, 295);
   
                                                         //frame.getContentPane().add(Class_booking_table_sp, BorderLayout.CENTER);
            
                                                         Logo = new JLabel();
                                                         Logo.setIcon(new ImageIcon(this.getClass().getResource("/img/vlc1.PNG")));
                                               	        
                                                         Logo.setBounds(998, 268, 202, 82);
                                                 
                                                                 frame.getContentPane().add(Logo);
	    } 
	      
	    public static void main(String[] args) { 
	        new StudentAttendance(); 
	       
	        
	        AttendanceData    Attendance1 = new AttendanceData("amy",   "Nile",   "badminton",  "Studio A", "Mon May 22 00:19:50 PKT 2019", "09:00:00", "am", "Present" );
	        AttendanceData    Attendance2 = new AttendanceData("zaid",  "hussam", "Swimmimg",   "pool",     "Tue May 23 00:19:50 PKT 2019", "10:00:00", "am", "Present" );
	        AttendanceData    Attendance3 = new AttendanceData("tuba",  "Amir",   "gym",        "gym",      "Wed May 23 00:19:50 PKT 2019", "11:00:00", "am", "Present" );
	        AttendanceData    Attendance4 = new AttendanceData("zahid", "nawal",  "gym",        "gym",      "Thu May 24 00:19:50 PKT 2019", "02:00:00", "pm", "Present" );
	        AttendanceData    Attendance5 = new AttendanceData("taha",  "hifza",  "Swimmimg",   "pool",     "Fri May 24 00:19:50 PKT 2019", "03:00:00", "pm", "Present" );
	        
	        
	        ArrayList <AttendanceData> list =  Singleton.getInstance().getattendancedataList();			
		    
		    list.add(Attendance1);
		    list.add(Attendance2);
		    list.add(Attendance3);
		    list.add(Attendance4);
		    list.add(Attendance5);
			
	        
	} 
}