package vlc;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.*;
import java.awt.print.*;
import java.util.ArrayList;

import javafx.print.*;

public class Report {
	 JFrame frame;
	 JMenuBar menubar3;
	 JMenu filemenu; 
	 JMenu openmenu;  JMenuItem studentregistration_mi;  JMenuItem coachregistration_mi;  JMenuItem appointment_mi;  JMenuItem feebilling_mi;  JMenuItem  classbokking_mi; JMenuItem Attendance_mi;
	 JMenu printmenu; JMenuItem Student_data_print;   JMenuItem Coach_data_print;  JMenuItem Appointment_data_print;  JMenuItem Fee_data_print; JMenuItem Class_booking_data_print;
	 JMenu Logoutmenu; JMenuItem Logout_mi;
	 JMenu Helpmenu; JMenuItem Help_mi;
	 JMenuItem Exit;
	 JLabel Records;    
	 JLabel Student_info;
	 JButton Load_student_info_btn;
	 JTable Student_table;
	 JScrollPane Student_table_sp;
	 JLabel Coach_info;
	 JButton Load_coach_info_btn;
	 JTable Coach_table;
	 JScrollPane Coach_table_sp;
	 JLabel Appointment_info;
	 JButton Load_appointment_info_btn;
	 JTable Appointment_table;
	 JScrollPane Appointment_table_sp;
	 JLabel Fee_info;
	 JButton Load_fee_info_btn;
	 JTable Fee_table;
	 JScrollPane Fee_table_sp;
	 JLabel Class_booking_info;
	 JButton Load_class_booking_info_btn;
	 JTable Class_booking_table;
	 JScrollPane Class_booking_table_sp;
	 JLabel Attendance_info;
	 JButton Attendance_info_btn;
	 JTable Attendance_table;
	 JScrollPane Attendance_table_sp;
	 JButton Back_btn;    	        
	 JButton Exit_btn; 
	 JTable table;
     JLabel Logo;
     DefaultTableModel model;
	  
	 Report() 
	    { 
	         
	        frame=new JFrame("Venue Leisure Centre"); 
	        Image img = Toolkit.getDefaultToolkit().getImage("Resources/img/vlcmin.PNG");
	        frame.setIconImage(img);
	        
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
	        
	        frame.setSize(1770, 847); 
	        
	        frame.setLocationRelativeTo(null);
	          
	        frame.getContentPane().setLayout(null);   
	          
	        frame.setVisible(true);
	        
	        menubar3 = new JMenuBar ();
	        frame.setJMenuBar (menubar3);
	        
	        filemenu = new JMenu ("File");
	        menubar3.add (filemenu);
	        
	       
	        
	        openmenu = new JMenu ("Open");
	        filemenu.add (openmenu);
	        openmenu.setIcon(new ImageIcon(this.getClass().getResource("/icon/open.png")));
	        
	        studentregistration_mi = new JMenuItem("Student Registration");
	        studentregistration_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Studentregistration.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(studentregistration_mi);
	        studentregistration_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/student reg jmi.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        coachregistration_mi = new JMenuItem("Coach Registration");
	        coachregistration_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Coachregistration.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(coachregistration_mi);
	        coachregistration_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/coach reg jmi.png"))); 
	        
	        openmenu.add(new JSeparator());
	        
	        appointment_mi = new JMenuItem("Appointment");
	        appointment_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Appointment.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(appointment_mi);
	        appointment_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/appointment jmi.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        feebilling_mi = new JMenuItem("Fee Billing");
	        feebilling_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Feebilling.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(feebilling_mi);
	        feebilling_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/fee billing jmi.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        classbokking_mi = new JMenuItem("Class Booking");
	        classbokking_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Classbooking.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(classbokking_mi);
	        classbokking_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/class booking jmi.png")));
	        
	        filemenu.add(new JSeparator());
	        
	        printmenu = new JMenu ("Print");
	        filemenu.add (printmenu);
	        printmenu.setIcon(new ImageIcon(this.getClass().getResource("/icon/printer.png")));
	        
 openmenu.add(new JSeparator());
	        
	        Attendance_mi = new JMenuItem("Attendance");
	        Attendance_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        	   StudentAttendance.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(Attendance_mi);
	        Attendance_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/Attendance jmi.png")));
	        
	        
	        filemenu.add(new JSeparator());
	        
	        Student_data_print = new JMenuItem("Print Student Info");
	        Student_data_print.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		
	        		try
		    		{
	        			Student_table.print();
		    		}
		    		catch(java.awt.print.PrinterException a)
		    		{
		    			System.err.format("no printer found", a.getMessage());
		    		}
	        		 
	        	}
	        });
	        printmenu.add(Student_data_print);
	        Student_data_print.setIcon(new ImageIcon(this.getClass().getResource("/icon/printer jmi.png")));
	        
	        printmenu.add(new JSeparator());
	        
	        Coach_data_print = new JMenuItem("Print Coach Info");
	        Coach_data_print.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		
	        		try
		    		{
	        			Coach_table.print();
		    		}
		    		catch(java.awt.print.PrinterException b)
		    		{
		    			System.err.format("no printer found", b.getMessage());
		    		}
	        		 
	        	}
	        });
	        printmenu.add(Coach_data_print);
	        Coach_data_print.setIcon(new ImageIcon(this.getClass().getResource("/icon/printer jmi.png")));
	        
	        printmenu.add(new JSeparator());
	        
	        Appointment_data_print = new JMenuItem("Print Appointment Info");
	        Appointment_data_print.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		
	        		try
		    		{
	        			Appointment_table.print();
		    		}
		    		catch(java.awt.print.PrinterException c)
		    		{
		    			System.err.format("no printer found", c.getMessage());
		    		}
	        		 
	        	}
	        });
	        printmenu.add(Appointment_data_print);
	        Appointment_data_print.setIcon(new ImageIcon(this.getClass().getResource("/icon/printer jmi.png")));
	        
	        printmenu.add(new JSeparator());
	        
	        Fee_data_print = new JMenuItem("Print Fee Info");
	        Fee_data_print.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		
	        		try
		    		{
	        			Fee_table.print();
		    		}
		    		catch(java.awt.print.PrinterException d)
		    		{
		    			System.err.format("no printer found", d.getMessage());
		    		}
	        		 
	        	}
	        });
	        printmenu.add(Fee_data_print);
	        Fee_data_print.setIcon(new ImageIcon(this.getClass().getResource("/icon/printer jmi.png")));
	        
	        printmenu.add(new JSeparator());
	        
	        Class_booking_data_print = new JMenuItem("Print Class Booking Info");
	        Class_booking_data_print.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		
	        		try
		    		{
	        			Class_booking_table.print();
		    		}
		    		catch(java.awt.print.PrinterException d)
		    		{
		    			System.err.format("no printer found", d.getMessage());
		    		}
	        		 
	        	}
	        });
	        printmenu.add(Class_booking_data_print);
	        Class_booking_data_print.setIcon(new ImageIcon(this.getClass().getResource("/icon/printer jmi.png")));
	        
	        Exit = new JMenuItem("Exit");
	        Exit.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
	        		 int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
		             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

		                    {
		                     System.exit(0);
		                    }
	        	}
	        });
	        filemenu.add(Exit);
	        Exit.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png")));
	        
	        Logoutmenu = new JMenu ("Logout");
	        menubar3.add (Logoutmenu);
	         
	        
	        Logout_mi = new JMenuItem("Logout");
	        Logout_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
	        		int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to logout?","Logout",JOptionPane.YES_NO_OPTION);
		             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 
		                    {
		            	 Loginselection.main(null);
			                 frame.dispose();
		                    }
	        	}
	        });
	        Logoutmenu.add(Logout_mi);
	        Logout_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/logout.png"))); 
	        
	        Helpmenu = new JMenu ("Help");
	        menubar3.add (Helpmenu);
	         
	        
	        Help_mi = new JMenuItem("Help");
	        Help_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Help.main(null); 
	        	}
	        });
	        
	        Helpmenu.add(Help_mi);
	        Help_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/help.png")));
	          
            Records = new JLabel("Records"); 
	        
                   Records.setBounds(726, 13, 58, 26); 
	          
	                      frame.getContentPane().add(Records); 
	        
	        Student_info = new JLabel("Student Info"); 
	        
	                    Student_info.setBounds(325, 41, 78, 26); 
	        	          
	        	                    frame.getContentPane().add(Student_info);
	        	        
	       /*Student_table = new JTable();
	        
	        	         Student_table.setBounds(12, 80, 721, 188);
	        			
	                                  frame.getContentPane().add(Student_table);
	        
	        Student_table_sp = new JScrollPane(Student_table);
	        
	                                    Student_table_sp.setBounds(12, 80, 721, 188);
	        
	                                                     frame.getContentPane().add(Student_table_sp, BorderLayout.CENTER);*/
	        
	                                                     Load_student_info_btn = new JButton("Load Student"); 
	                                                     Load_student_info_btn.addActionListener(new ActionListener() {
	                                                     	public void actionPerformed(ActionEvent arg0) {
	                                                     		ArrayList<StudentregistrationData> vector = Singleton.getInstance().getstudentregistrationdataList();
	                                   		   		            String[] columnNames = {"Student Id", "First Nmae", "Last Name", "Guardians First Name", "Guardians Last Name", "Date of Birth", "Date of Admission", "Gender", "Blood Group", "Email", "Telephone No", "Emergency Contact", "Street No", "House No", "Zip Code", "Class to Attend", "Alloted Room"}; 

	                                   		   		  	 Object[][] data;
	                                   		   		  	 
	                                   					data = new Object[vector.size()][18];

	                                   					for (int i = 0; i < data.length; i++) {
	                                   						StudentregistrationData a = vector.get(i);
	                                   						data[i][0] = a.getStudent_id();
	                                   						data[i][1] = a.getFname();
	                                   						data[i][2] = a.getLname();
	                                   						data[i][3] = a.getGuardians_First();
	                                   						data[i][4] = a.getGuardians_Last();
	                                   						data[i][5] = a.getDate_of_birth_dc();
	                                   						data[i][6] = a.getDate_of_admission_dc();
	                                   						data[i][7] = a.getGender_bg();
	                                   						data[i][8] = a.getBlood_group_cb();
	                                   						data[i][9] = a.getEmail();
	                                   						data[i][10] = a.getTelephone_no();
	                                   						data[i][11] = a.getEmergency_contact();
	                                   						data[i][12] = a.getStreet_no();
	                                   						data[i][13] = a.getHouse_no();
	                                   						data[i][14] = a.getZip_code();
	                                   						data[i][15] = a.getClass_to_attend_cb();
	                                   						data[i][16] = a.getAlloted_room_cb();
	                                   						
	                                   					
	                                 
	                                   						;
	                                   						
	                                   						
	                                   						
	                                   					}
	                                   		   				model = new DefaultTableModel(data, columnNames);
	                                   		   			Student_table = new JTable(model);
	                                   		   		Student_table_sp = new JScrollPane(Student_table);

	                                   		   	Student_table_sp.setVisible(true);

	                                   		 Student_table_sp.setBounds(12, 80, 721, 188);

	                                	   	                     frame.getContentPane().add(Student_table_sp);	
	                                   		                  
	                                   		                 }
	                                   		                 
	                                                   });	
	                                                     	
	                                                     
	                                                     Load_student_info_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/load data.png")));
	                                          	        	        	        	                    
	                                                     Load_student_info_btn.setBounds(1524, 80, 202, 33);
	                                          	        	        	        	                    	   	          
	                                          	                                  frame.getContentPane().add(Load_student_info_btn);
	                                                     
	        Coach_info = new JLabel("Coach Info"); 
	        	        
	        	      Coach_info.setBounds(1099, 51, 67, 26); 
	        	        	          
	        	        	     frame.getContentPane().add(Coach_info);
	        	        	        
	        /*Coach_table = new JTable();
	        	        	   		
	        	       Coach_table.setBounds(773, 80, 721, 188);
	        	        			
	        	                   frame.getContentPane().add(Coach_table);
	        	                   
	        Coach_table_sp = new JScrollPane(Coach_table);
	        	       	        
	                                  Coach_table_sp.setBounds(773, 80, 721, 188);
		        
		                                             frame.getContentPane().add(Coach_table_sp, BorderLayout.CENTER);*/
	        	                    
		                                             Load_coach_info_btn= new JButton("Load Coach"); 
		                                             Load_coach_info_btn.addActionListener(new ActionListener() {
		                                             	public void actionPerformed(ActionEvent arg0) {
		                                             		ArrayList<CoachregistrationData> vector = Singleton.getInstance().getcoachregistrationdataList();
		                            	   		            String[] columnNames = {"Coach Id", "First Name", "Last Name", "Fathers Name", "Date of Birth", "Date of Hire", "Gender", "Blood Group", "Basic Medical Tranning", "Icf Certified", "Certifications Aquired", "Educational Background", "Email", "Telephone No", "Emergency Contact", "Street No", "House No", "Zip Code", "Coaches Salary", "Previous Experience", "Class to Teach", "Alloted Room"}; 

		                            	   		  	 Object[][] data;
		                            	   		  	 
		                            				data = new Object[vector.size()][22];

		                            				for (int i = 0; i < data.length; i++) {
		                            					CoachregistrationData a = vector.get(i);
		                            					data[i][0] = a.getCoach_id();
		                            					data[i][1] = a.getFirst_name();
		                            					data[i][2] = a.getLast_name();
		                            					data[i][3] = a.getFather_name();
		                            					data[i][4] = a.getDate_of_birth_dc();
		                            					data[i][5] = a.getDate_of_hire_dc();
		                            					data[i][6] = a.getGender_bg();
		                            					data[i][7] = a.getBlood_group_cb();
		                            					data[i][8] = a.getBasic_medical_tranning_bg();
		                            					data[i][9] = a.getIcf_certified_bg();
		                            					data[i][10] = a.getCertifications_aquired();
		                            					data[i][11] = a.getEducational_background();
		                            					data[i][12] = a.getEmail();
		                            					data[i][13] = a.getTelephone_no();
		                            					data[i][14] = a.getEmergency_contact();
		                            					data[i][15] = a.getStreet_no();
		                            					data[i][16] = a.getHouse_no();
		                            					data[i][17] = a.getZip_code();
		                            					data[i][18] = a.getCoaches_salary();
		                            					data[i][19] = a.getPrevious_experience_cb();
		                            					data[i][20] = a.getClass_to_teach_cb();
		                            					data[i][21] = a.getAlloted_room_cb();
		                            					

		                            					
		                            					
		                            					

		                            	               
		                            					
		                            					
		                            				}
		                            	   				model = new DefaultTableModel(data, columnNames);
		                            	   				Coach_table = new JTable(model);
		                            	   				Coach_table_sp  = new JScrollPane(Coach_table);

		                            	   				Coach_table_sp.setVisible(true);

		                            	   				Coach_table_sp.setBounds(773, 80, 721, 188);

		                            	                     frame.getContentPane().add(Coach_table_sp);	
		                            	                  
		                            	                 }
		                            	                 
		                                        });		
		                                             	
		                                             
		                                             Load_coach_info_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/load data.png")));
		                                  	        	        	        	                    
		                                             Load_coach_info_btn.setBounds(1524, 126, 202, 33);
		                                  	        	        	        	                    	   	          
		                                  	                                  frame.getContentPane().add(Load_coach_info_btn);
		                                             
	        Appointment_info = new JLabel("Appointment Info"); 
	        	        	        
	        	        	Appointment_info.setBounds(299, 281, 104, 26); 
	        	        	        	          
	        	        	        	     frame.getContentPane().add(Appointment_info);
	        	        	        	        
	        	        	        	     /*Appointment_table = new JTable();
	        	        	     	        
	        	        	        	     Appointment_table.setBounds(12, 320, 721, 188);
	        	    	        			
	        	    	                                  frame.getContentPane().add(Appointment_table);
	        	    	        
	        	    	                                  Appointment_table_sp = new JScrollPane(Appointment_table);
	        	    	        
	        	    	                                  Appointment_table_sp.setBounds(12, 320, 721, 188);
	        	    	        
	        	    	                                                     frame.getContentPane().add(Appointment_table_sp, BorderLayout.CENTER);*/
	        	        	        	        
	     		                                                Load_appointment_info_btn = new JButton("Load Appointment"); 
	     		                                                Load_appointment_info_btn.addActionListener(new ActionListener() {
	     		                                                	public void actionPerformed(ActionEvent arg0) {
	     		                                                		 ArrayList<AppointmentData> vector = Singleton.getInstance().getappointmentdataList();
	     		                                                		String[] columnNames = { "Parent Name", "Student Name", "Gender", "Telephone No", "Email", "Date of Appointment", "Day of Appointment", "Time From", "Time To", "Coach to Meet"};

	     		        	        		   		   		  	 Object[][] data;
	     		        	        		   		   		  	 
	     		        	        		   					data = new Object[vector.size()][11];

	     		        	        		   					for (int i = 0; i < data.length; i++) {
	     		        	        		   						AppointmentData a = vector.get(i);
	     		        	        		   					data[i][0] = a.getParent_name_tf();
			        	        		   						data[i][1] = a.getStudent_name_tf();
			        	        		   						data[i][2] = a.getGender_bg();
			        	        		   						data[i][3] = a.getTelephone();
			        	        		   						data[i][4] = a.getEmail();
			        	        		   						data[i][5] = a.getAppointment_date_dc();
			        	        		   						data[i][6] = a.getDay_of_appointment_cb();
			        	        		   						data[i][7] = a.getTime_from_tc();
			        	        		   						data[i][8] = a.getTime_to_tc();
			        	        		   						data[i][9] = a.getCoach_meet_cb();
	     		        	        		   						
	     		        	        		   						
	     		        	        		   						
	     		        	        		   					}
	     		        	        		   		   				model = new DefaultTableModel(data, columnNames);
	     		        	        		   		                  Appointment_table = new JTable(model);
	     		        	        		   		              Appointment_table_sp  = new JScrollPane(Appointment_table);

	     		        	        		   		          Appointment_table_sp.setVisible(true);

	     		        	        		   		      Appointment_table_sp.setBounds(12, 320, 721, 188);

	     			        	        		   	                     frame.getContentPane().add(Appointment_table_sp);	
	     		        	        		   		                  
	     		        	        		   		                 }
	     		                                                	
	     		                                                });
	     		                                                	
	     		                                               
	     		                                               Load_appointment_info_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/load data.png")));
	     		                                    	        	        	        	                    
	     		                                              Load_appointment_info_btn.setBounds(1524, 172, 202, 33);
	     		                                    	        	        	        	                    	   	          
	     		                                    	                                  frame.getContentPane().add(Load_appointment_info_btn);
	     		                                                 
	        Fee_info = new JLabel("Fee Info"); 
	        	        	        	        
	        	    Fee_info.setBounds(1099, 281, 67, 26); 
	        	        	        	        	          
	        	        	frame.getContentPane().add(Fee_info);
	        	        	        	        	        
	        /*Fee_table = new JTable();
	        	        	        	        	   		
	        	      Fee_table.setBounds(773, 320, 721, 188);
	        	        	        	        			
	        	        	    frame.getContentPane().add(Fee_table);
	        	        	    
	        Fee_table_sp = new JScrollPane(Fee_table);
	        	       	        
	        	        	        Fee_table_sp.setBounds(773, 320, 721, 188);
		        
		                                         frame.getContentPane().add(Fee_table_sp, BorderLayout.CENTER);*/

		   Load_fee_info_btn = new JButton("Load fee"); 
		   Load_fee_info_btn.addActionListener(new ActionListener() {
		   	public void actionPerformed(ActionEvent e) {
		   	 ArrayList<FeebillingData> vector = Singleton.getInstance().getfeebillingdataList();
		            String[] columnNames = {"Student Id", "First Nmae", "Last Name", "Payment Type", "Card Number", "Cvv","Credit card Type", "Valid From", "Valid To", "Payment Date", "Payment Time", "am/pm", "Fees Paid(Rs)", "fees Due(Rs)"}; 

		  	 Object[][] data;
		  	 
		data = new Object[vector.size()][14];

		for (int i = 0; i < data.length; i++) {
			FeebillingData a = vector.get(i);
			data[i][0] = a.getStudent_id();
			data[i][1] = a.getFname();
			data[i][2] = a.getLname();
			data[i][3] = a.getPayment_bg();
			data[i][4] = a.getCardnum();
			data[i][5] = a.getCvv();
			data[i][6] = a.getCreditcard_cb();
			data[i][7] = a.getValidfrom_dc();
			data[i][8] = a.getValidto_dc();
			data[i][9] = a.getPaymentdate_dc();
			data[i][10] = a.getPaymenttime_tc();
			data[i][11] = a.getPaymenttime_cb();
			data[i][12] = a.getFeespaid();
			data[i][13] = a.getFeesdue();
			
			
			
			

         
			
			
		}
				model = new DefaultTableModel(data, columnNames);
				Fee_table = new JTable(model);
				Fee_table_sp  = new JScrollPane(Fee_table);

				Fee_table_sp.setVisible(true);

				Fee_table_sp.setBounds(773, 320, 721, 188);

                 frame.getContentPane().add(Fee_table_sp);	
              
             }
             
    });
		   	
           
		   Load_fee_info_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/load data.png")));
	        	        	        	                    
		   Load_fee_info_btn.setBounds(1524, 218, 202, 33);
	        	        	        	                    	   	          
	                                  frame.getContentPane().add(Load_fee_info_btn);
	                                  
	                                  Class_booking_info = new JLabel("Class booking info"); 
	          	        	        
	                                  Class_booking_info.setBounds(299, 515, 124, 26); 
	            	        	        	          
	            	        	        	     frame.getContentPane().add(Class_booking_info);
	                                  
	                                  
	                                  /*Class_booking_table = new JTable();
	                      	        
	                                  Class_booking_table.setBounds(393, 574, 721, 188);
	         	        			
	         	                                  frame.getContentPane().add(Class_booking_table);
	         	        
	         	                                 Class_booking_table_sp = new JScrollPane(Class_booking_table);
	         	        
	         	                                Class_booking_table_sp.setBounds(393, 574, 721, 188);
	         	        
	         	                                                     frame.getContentPane().add(Class_booking_table_sp, BorderLayout.CENTER);*/
	                                  
	                                  
	                               Load_class_booking_info_btn = new JButton("Load Class booking info"); 
	                               Load_class_booking_info_btn.addActionListener(new ActionListener() {
	                               	public void actionPerformed(ActionEvent arg0) {
	                               		ArrayList<ClassbookingData> vector = Singleton.getInstance().getclassbookingdataList();
	            	   		            String[] columnNames = { "Student id", "Coach id", "First Name", "Last Name", "Class", "Date", "Time", "am/pm"}; 

	            	   		  	 Object[][] data;
	            	   		  	 
	            				data = new Object[vector.size()][10];

	            				for (int i = 0; i < data.length; i++) {
	            						ClassbookingData a = vector.get(i);
	            						data[i][0] = a.getStudent_id();
	            						data[i][1] = a.getCoach_id();
	            						data[i][2] = a.getFirst();
	            						data[i][3] = a.getLast();
	            						data[i][4] = a.getClass_cb();
	            						data[i][5] = a.getRoom_cb();
	            						data[i][6] = a.getDate_dc();
	            						data[i][7] = a.getTime_tc();
	            						data[i][8] = a.getTime_cb();
	            					
	            			
	            					
	            					
	            				}
	            	   				model = new DefaultTableModel(data, columnNames);
	            	   				Class_booking_table = new JTable(model);
	            	   				Class_booking_table_sp = new JScrollPane(Class_booking_table);

	            	   				Class_booking_table_sp.setVisible(true);

	            	   				Class_booking_table_sp.setBounds(10, 552, 721, 188);

	            	                     frame.getContentPane().add(Class_booking_table_sp);	
	            	                  
	            	                 }
	            	                 
	                          });
	                               		
	                               
	                                  
	                               Load_class_booking_info_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/load data.png")));
	                       	        	        	        	                    
	                               Load_class_booking_info_btn.setBounds(1524, 263, 202, 33);
	                       	        	        	        	                    	   	          
	                       	                                  frame.getContentPane().add(Load_class_booking_info_btn);
	                       	                                  
	                       	                               
	                       	                                  
	                       	                               Attendance_info = new JLabel("Attendance info"); 
	                       	    	        	        
	                       	                            Attendance_info.setBounds(1099, 515, 96, 26); 
	                       	      	        	        	          
	                       	      	        	        	     frame.getContentPane().add(Attendance_info);
	                       	      	        	        	     
	                       	      	        	        	     
	                       	      	        	        	     
	                       	      	        	        	     
	                       	      	        	       Attendance_info_btn = new JButton("Load Attendance info"); 
	                       	      	        	       Attendance_info_btn.addActionListener(new ActionListener() {
	                       	      	        	       	public void actionPerformed(ActionEvent arg0) {
	                       	      	        	       ArrayList<AttendanceData> vector = Singleton.getInstance().getattendancedataList();
	                       		   		            String[] columnNames = {"Coach Name", "Student Name", "Class", "Room", "Date", "Time", "am/pm", "Attendance"}; 

	                       		   		  	 Object[][] data;
	                       		   		  	 
	                       					data = new Object[vector.size()][9];

	                       					for (int i = 0; i < data.length; i++) {
	                       							AttendanceData a = vector.get(i);
	                       						
	                       						data[i][0] = a.getCoach_name_tf();
	                       						data[i][1] = a.getStudent_name_tf();
	                       						data[i][2] = a.getClass_cb();
	                       						data[i][3] = a.getRoom_cb();
	                       						data[i][4] = a.getDate_dc();
	                       						data[i][5] = a.getTime_tc();
	                       						data[i][6] = a.getTime_cb();
	                       						data[i][7] = a.getAttendance_bg();
	                       						
	                       				
	                       						
	                       						
	                       					}
	                       		   				model = new DefaultTableModel(data, columnNames);
	                       		   				Attendance_table = new JTable(model);
	                       		   				Attendance_table_sp = new JScrollPane(Attendance_table);

	                       		   				Attendance_table_sp.setVisible(true);

	                       		   				Attendance_table_sp.setBounds(773, 552, 721, 188);

	                       		                     frame.getContentPane().add(Attendance_table_sp);	
	                       	      	        	       	}
	                       	      	        	       });
	                       	      	                   
	                       	      			             
	                       	      	        	       Attendance_info_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/load data.png")));
	                       	      	                   
	                       	      	        	  Attendance_info_btn.setBounds(1524, 311, 202, 33);
	                       	      		   				          
	                       	      		   		       frame.getContentPane().add(Attendance_info_btn);
	                       	      	        	        	     
	                       	      	        	        	     
	                                  
	        	        	        	                    
           Back_btn = new JButton("Back"); 
                   Back_btn.addActionListener(new ActionListener() {
	                  public void actionPerformed(ActionEvent arg0) {
		              Mainpage.main(null);
		              frame.dispose();
	}
});
           Back_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/back.png")));
	   	        
           Back_btn.setBounds(1524, 355, 202, 33);
	   	          
	   	            frame.getContentPane().add(Back_btn);
	   		        
           Exit_btn = new JButton("Exit"); 
                   Exit_btn.addActionListener(new ActionListener() {
	                  public void actionPerformed(ActionEvent e) {
		              int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
		              if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

		                       {
		                        System.exit(0);
		                       }
	}
});
          Exit_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png")));
                   
          Exit_btn.setBounds(1524, 401, 202, 33);
	   				          
	   		       frame.getContentPane().add(Exit_btn);
	   		       
	   		    Logo = new JLabel();
	   		    
	   		    Logo.setIcon(new ImageIcon(this.getClass().getResource("/img/vlc1.PNG")));
      	        
                Logo.setBounds(1538, 679, 202, 82);
        
                        frame.getContentPane().add(Logo);
	   		

			
	    } 
	      
	    public static void main(String[] args) { 
	        new Report(); 
	        
	    } 
	} 