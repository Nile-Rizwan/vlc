package vlc;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import com.toedter.calendar.JDateChooser;

import lu.tudor.santec.jtimechooser.JTimeChooser;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Vector;

public class Appointment {
	 JFrame frame; 
	 JMenuBar menubar4;
	 JMenu filemenu; JMenuItem Exit;
	 JMenu openmenu; JMenuItem studentregistration_mi;  JMenuItem coachregistration_mi;  JMenuItem records_mi;  JMenuItem feebilling_mi;  JMenuItem  classbokking_mi; JMenuItem Attendance_mi;
	 JMenu Logoutmenu; JMenuItem Logout_mi;
	 JMenu Helpmenu; JMenuItem Help_mi;
	 JLabel Appointment; 
	 JLabel Parent_name;           JTextField Parent_name_tf;  
	 JLabel Student_name;            JTextField Student_name_tf;  
	 JLabel Gender;               JRadioButton Male_rbtn;   JRadioButton Female_rbtn;   JRadioButton Other_rbtn;
	 ButtonGroup Gender_bg;
	 JLabel Telephone_no;         JTextField Telephone_no_tf;
	 JLabel Email;                JTextField Email_tf;
	
	 JLabel Date_of_appointment;  JDateChooser Date_of_appointment_dc;
	 JLabel Day_of_appointment;   JComboBox Day_of_appointment_cb;
	 JLabel Time_from;  JTimeChooser Time_from_tc; 
	 JLabel Time_to;  JTimeChooser Time_to_tc;
	 JLabel Coach_to_meet;        JComboBox Coach_to_meet_cb;
	 JButton Back_btn;    	        
	 JButton Clear_btn;    		        
	 JButton Save_btn; 
	 JButton Exit_btn; 
	 JLabel Search;
	 JTextField Search_tf;
	 JButton Load_data_btn;
	 JTable Appointment_table;
	 JScrollPane Appointment_table_sp;
     JLabel Logo;
 	DefaultTableModel model;
 	JScrollPane scrollPane; 
	 Appointment() 
	    { 
	         //frame code
	        frame=new JFrame("Venue Leisure Centre"); 
	        Image img = Toolkit.getDefaultToolkit().getImage("Resources/img/vlcmin.PNG");
	        frame.setIconImage(img);
	        
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
	        
	        frame.setSize(1330, 597); 
	        
	        frame.setLocationRelativeTo(null);
	          
	        frame.getContentPane().setLayout(null);   
	          
            frame.setVisible(true);
            
            menubar4 = new JMenuBar ();
	        frame.setJMenuBar (menubar4);
	        
	        filemenu = new JMenu ("File");
	        menubar4.add (filemenu);
	        
	        
	        
	        //Menu Bar
	        openmenu = new JMenu ("Open");
	        filemenu.add (openmenu);
	        openmenu.setIcon(new ImageIcon(this.getClass().getResource("/icon/open.png"))); 
	        
	        studentregistration_mi = new JMenuItem("Student Registration");
	        studentregistration_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Studentregistration.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(studentregistration_mi);
	        studentregistration_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/student reg jmi.png"))); 
	        
	        openmenu.add(new JSeparator());
	        
	        coachregistration_mi = new JMenuItem("Coach Registration");
	        coachregistration_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Coachregistration.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(coachregistration_mi);
	        coachregistration_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/coach reg jmi.png"))); 
	        
	        openmenu.add(new JSeparator());
	        
	        records_mi = new JMenuItem("Records");
	        records_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Report.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(records_mi);
	        records_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/records jmi.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        feebilling_mi = new JMenuItem("Fee Billing");
	        feebilling_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Feebilling.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(feebilling_mi);
	        feebilling_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/fee billing jmi.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        classbokking_mi = new JMenuItem("Class Booking");
	        classbokking_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Classbooking.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(classbokking_mi);
	        classbokking_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/class booking jmi.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        Attendance_mi = new JMenuItem("Attendance");
	        Attendance_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        	   StudentAttendance.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(Attendance_mi);
	        Attendance_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/Attendance jmi.png")));
	        
	        
	        
	        filemenu.add(new JSeparator());
	        
	        Exit = new JMenuItem("Exit");
	        Exit.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
	        		 int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
		             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

		                    {
		                     System.exit(0);
		                    }
	        	}
	        });
	        filemenu.add(Exit);
	        Exit.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png"))); 
	        
	        Logoutmenu = new JMenu ("Logout");
	        menubar4.add (Logoutmenu);
	         
	        
	        Logout_mi = new JMenuItem("Logout");
	        Logout_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
	        		int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to logout?","Logout",JOptionPane.YES_NO_OPTION);
		             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 
		                    {
		            	     Loginselection.main(null);
			                 frame.dispose();
		                    }
	        	}
	        });
	        Logoutmenu.add(Logout_mi);
	        Logout_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/logout.png"))); 
	        
	        Helpmenu = new JMenu ("Help");
	        menubar4.add (Helpmenu);
	         
	        
	        Help_mi = new JMenuItem("Help");
	        Help_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Help.main(null); 
	        	}
	        });
	        
	        Helpmenu.add(Help_mi);
	        Help_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/help.png")));
	        
	        
	        //JLabels and Text fields
	          
            Appointment = new JLabel("Appointment"); 
	        
                       Appointment.setBounds(426, 13, 126, 26); 
	          
	                               frame.getContentPane().add(Appointment);
	       	        	          
            Parent_name = new JLabel("Parent Name"); 
	        
                      Parent_name.setBounds(12, 94, 75, 26); 
	          
	                            frame.getContentPane().add(Parent_name);
	
            Parent_name_tf = new JTextField(); 
	        
                         Parent_name_tf.setBounds(161, 96, 210, 22);
	          
	                                   frame.getContentPane().add(Parent_name_tf);

            Student_name = new JLabel("Student Name"); 
	        
                     Student_name.setBounds(12, 133, 75, 26); 
	          
	                          frame.getContentPane().add(Student_name);
	
            Student_name_tf = new JTextField(); 
	        
                        Student_name_tf.setBounds(161, 135, 210, 22);
	          
	                                frame.getContentPane().add(Student_name_tf);
	        
            Gender = new JLabel("Gender"); 
	        
                  Gender.setBounds(12, 172, 51, 26); 
	        	          
	                     frame.getContentPane().add(Gender);
	        	
            Male_rbtn = new JRadioButton("Male");
            
            Male_rbtn.setActionCommand("Male");

                     Male_rbtn.setBounds(161, 175, 64, 23);
	                    
	                           frame.getContentPane().add(Male_rbtn);

            Female_rbtn = new JRadioButton("Female");
            
            Female_rbtn.setActionCommand("Female");

                       Female_rbtn.setBounds(229, 175, 75, 23);
	                     
	                              frame.getContentPane().add(Female_rbtn);
	                      
            Other_rbtn = new JRadioButton("Other");
            
            Other_rbtn.setActionCommand("Other");

                      Other_rbtn.setBounds(308, 174, 63, 23);
      
	                            frame.getContentPane().add(Other_rbtn);
	        
	        Gender_bg = new ButtonGroup();
	        Gender_bg.add(Male_rbtn);
	        Gender_bg.add(Female_rbtn);
	        Gender_bg.add(Other_rbtn);
	        
	        Telephone_no = new JLabel("Telephone No"); 
	        
	                    Telephone_no.setBounds(12, 211, 103, 22); 
	        	          
	        	                     frame.getContentPane().add(Telephone_no);
	        	
	        Telephone_no_tf = new JTextField(); 
	        	        
	        	           Telephone_no_tf.setBounds(161, 211, 210, 22);
	        
	        	                          frame.getContentPane().add(Telephone_no_tf);
	        
	        Email = new JLabel("Email"); 
	        
	             Email.setBounds(12, 246, 75, 26); 
	        	          
	        	       frame.getContentPane().add(Email);
	        	
	        Email_tf = new JTextField(); 
	        	        
	        	    Email_tf.setBounds(161, 248, 210, 22);
	        
	                        frame.getContentPane().add(Email_tf);
	             
          
	                                                    
	        Date_of_appointment = new JLabel("Date Of Appointment");
	                                        	        
	                           Date_of_appointment.setBounds(12, 285, 137, 26);
	                     	        	        
	                     	                       frame.getContentPane().add( Date_of_appointment);
	                     	                                              
	        Date_of_appointment_dc = new JDateChooser();
	        
	                     	      Date_of_appointment_dc.setBounds(161, 291, 210, 20);

	              	        	        	        	 frame.getContentPane().add(Date_of_appointment_dc);
	              	        	        	        	 
	              	        	        	        	  Day_of_appointment = new JLabel("Day Of Appointment");
	              	        	        		        
	              	                                    Day_of_appointment.setBounds(12, 322, 137, 26);
	              	     	        	        
	              	     	                                              frame.getContentPane().add( Day_of_appointment);
	              	           	        	        	        		         
	              	           	    Day_of_appointment_cb = new JComboBox();
	              	        	        	        	        	        
	              	           	        	        	  Day_of_appointment_cb.setModel(new DefaultComboBoxModel(new String[] {"Monday Evening", "Saturday Morning"}));
	              	        	        	        	        	        
	              	           	        	        	  Day_of_appointment_cb.setBounds(161, 322, 126, 26);
	              	        	        	        	        			
	              	        	        	        	        	                 frame.getContentPane().add(Day_of_appointment_cb);
	              	        	        	        	        	                 
	              	        	        	        	        	               Time_from = new JLabel("Time From");
	              	        	        	     	                 	        
	              	        	        	  	                               Time_from.setBounds(12, 370, 137, 26);
	              	        	        	  		        	        
	              	        	        	  		                                              frame.getContentPane().add( Time_from);
	              	        	        	  		        
	              	        	        	  		        Time_from_tc = new JTimeChooser();
	              	        	        	  		        
	              	        	        	  		                              Time_from_tc.setBounds(161, 370, 126, 26);
	              	        	        	  		                              
	              	        	        	  	      	        	        	        		         frame.getContentPane().add(Time_from_tc);
	              	        	        	  	      	        	        	        		         
	              	        	        	  	      	        	        	        		       Time_to = new JLabel("Time To");
	              	        	        	  	      	        	        	        		        
	              	        	        	  	      	        	                                 Time_to.setBounds(12, 411, 137, 26);
	              	        	        	  	      	        	  	        	        
	              	        	        	  	      	        	  	                                              frame.getContentPane().add( Time_to);
	              	        	        	  	      	        	  	        
	              	        	        	  	      	        	  	        Time_to_tc = new JTimeChooser();
	              	        	        	  	      	        	  	        
	              	        	        	  	      	        	  	                              Time_to_tc.setBounds(161, 411, 126, 26);
	              	        	        	  	      	        	  	                              
	              	        	        	  	      	        	        	        	        	        		         frame.getContentPane().add(Time_to_tc);
            
	        Coach_to_meet = new JLabel("Coach To Meet");
	        	        
	        	         Coach_to_meet.setBounds(12, 447, 97, 26);
	        	        
	        	                      frame.getContentPane().add(Coach_to_meet);
	        	        	        	        
	        Coach_to_meet_cb = new JComboBox();
	        	        
	        	            Coach_to_meet_cb.setModel(new DefaultComboBoxModel(new String[] {"Swimming", "badminton", "gym"}));
	        	        	        
	        	            Coach_to_meet_cb.setBounds(161, 448, 87, 26);
	        	        			
	        	        	                 frame.getContentPane().add(Coach_to_meet_cb);
	        	        	                 
	        	        	                 Load_data_btn = new JButton("Load Data"); 
	        	        	                 
	        	        	                 Load_data_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/load data.png"))); 
	        	 		                    
	        	 	                        Load_data_btn.setBounds(1098, 53, 124, 33);
	        	 		        	        	        	                    	   	          
	        	 		                                  frame.getContentPane().add(Load_data_btn);

		        	        		   		               //Loading code for array list

	        	 		                                 Load_data_btn.addActionListener(new ActionListener() {
		        	        		   		                 public void actionPerformed(ActionEvent e) {
		        	        		   		                	 ArrayList<AppointmentData> vector = Singleton.getInstance().getappointmentdataList();
		        	        		   		   		            String[] columnNames = { "Parent Name", "Student Name", "Gender", "Telephone No", "Email", "Date of Appointment", "Day of Appointment", "Time From", "Time To", "Coach to Meet"};

		        	        		   		   		  	 Object[][] data;
		        	        		   		   		  	 
		        	        		   					data = new Object[vector.size()][11];

		        	        		   					for (int i = 0; i < data.length; i++) {
		        	        		   						AppointmentData a = vector.get(i);
		        	        		   						
		        	        		   						data[i][0] = a.getParent_name_tf();
		        	        		   						data[i][1] = a.getStudent_name_tf();
		        	        		   						data[i][2] = a.getGender_bg();
		        	        		   						data[i][3] = a.getTelephone();
		        	        		   						data[i][4] = a.getEmail();
		        	        		   						data[i][5] = a.getAppointment_date_dc();
		        	        		   						data[i][6] = a.getDay_of_appointment_cb();
		        	        		   						data[i][7] = a.getTime_from_tc();
		        	        		   						data[i][8] = a.getTime_to_tc();
		        	        		   						data[i][9] = a.getCoach_meet_cb();
		        	        		   						
		        	        		   						
		        	        		   						
		        	        		   					}
		        	        		   		   				model = new DefaultTableModel(data, columnNames);
		        	        		   		                  Appointment_table = new JTable(model);
		        	        		   		               Appointment_table_sp  = new JScrollPane(Appointment_table);

				        	        		   		        	Appointment_table_sp.setVisible(true);

				        	        		   		        	Appointment_table_sp.setBounds(383, 54, 703, 338);

			        	        		   	                     frame.getContentPane().add(Appointment_table_sp);	
		        	        		   		                  
		        	        		   		                 }
		        	        		   		                 
	        	 		                                 });

	        	        	                 Save_btn = new JButton("Save"); 
	        	        	                 
	        	        	                 Save_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/save.png")));
	        	     	   			        
	        	        	                    Save_btn.setBounds(1098, 91, 124, 33);
	        	        		   			          
	        	        		   	                     frame.getContentPane().add(Save_btn);	
       
	        	        		   	                  Save_btn.addActionListener(new ActionListener() {
	        	        		   	                	  
	        	        		   	                	  //Save button for array list
	        	        		   		                 public void actionPerformed(ActionEvent e) {
	        	        		   		                	 ArrayList<AppointmentData> al = Singleton.getInstance().getappointmentdataList();
	        	        		   		                	 AppointmentData app = new AppointmentData(Parent_name_tf.getText(), Student_name_tf.getText(), Gender_bg.getSelection().getActionCommand(), Telephone_no_tf.getText(), Email_tf.getText(), Date_of_appointment_dc.getDate().toString(), Day_of_appointment_cb.getSelectedItem().toString(), Time_from_tc.getTimeField().getText(), Time_to_tc.getTimeField().getText(), Coach_to_meet_cb.getSelectedItem().toString() );
   	        		   		                	 
	        	        		   		                	 al.add(app);
	        	        		   		                	JOptionPane.showMessageDialog(null, "Data has been successfully inserted");   
	        	        		   		                 }
	        	        		   	});
	        	        		   	                     
	        	        		   	                     
	       Back_btn = new JButton("Back"); 
           Back_btn.addActionListener(new ActionListener() {
	                 public void actionPerformed(ActionEvent e) {
		             Mainpage.main(null);
		             frame.dispose();
	}
});
           Back_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/back.png"))); 
           
           Back_btn.setBounds(1098, 130, 124, 33);
	   	          
	   	            frame.getContentPane().add(Back_btn);
	   	            
	   	            //Clear Button
	   	        
          Clear_btn = new JButton("CLear"); 
                   Clear_btn.addActionListener(new ActionListener() {
	                 public void actionPerformed(ActionEvent e) {
		
		             
		  
		             Parent_name_tf.setText("");   
		  
		             Student_name_tf.setText("");   
		 
		             Gender_bg.clearSelection();
		 
		             Telephone_no_tf.setText(""); 
		 
		             Email_tf.setText(""); 
		             
		      
		
		             Date_of_appointment_dc.setDate(null);
		 
		             Coach_to_meet_cb.setSelectedIndex(0);
		             
		             Time_from_tc.getTimeField().setValue("00:00:00");
		             
		             Time_to_tc.getTimeField().setValue("00:00:00");
	}
	
});
	   		        
                   Clear_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/clear.png"))); 
                   
                   Clear_btn.setBounds(1098, 169, 124, 33);
	   		          
	   		                 frame.getContentPane().add(Clear_btn);
	   		               
	   		                 //Exit Button

           Exit_btn = new JButton("Exit"); 
                   Exit_btn.addActionListener(new ActionListener() {
	                 public void actionPerformed(ActionEvent e) {
		             int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
		             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

		                    {
		                     System.exit(0);
		                    }
	}
});
                   Exit_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png"))); 
	   				        
                   Exit_btn.setBounds(1098, 206, 124, 33);
	   		  		          
	   		               frame.getContentPane().add(Exit_btn);
	   		               
	   		            
	   		            Search = new JLabel("Search"); 
	   			        
	   		         Search.setBounds(719, 30, 75, 26); 
		          
		                          frame.getContentPane().add(Search);
		                          
		                          
		                          //Search Button
		
		                          Search_tf = new JTextField(); 
		                          Search_tf.addKeyListener(new KeyAdapter() {
		                          	@Override
		                          	public void keyReleased(KeyEvent e) {
		                          		 DefaultTableModel model = (DefaultTableModel)Appointment_table.getModel();
		                                 String search = Student_name_tf.getText();
		                                 TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(model);
		                                 Appointment_table.setRowSorter(tr);
		                                 tr.setRowFilter(RowFilter.regexFilter(search));	
		                          	}
		                          });
		        
		                          Search_tf.setBounds(806, 32, 210, 22);
		          
		                                frame.getContentPane().add(Search_tf);
	   		
			
             //          frame.getContentPane().add(Appointment_table);
                       
            //Appointment_table_sp = new JScrollPane(Appointment_table);
      	              
               //                 Appointment_table_sp.setBounds(383, 55, 541, 335);
   
                                       //              frame.getContentPane().add(Appointment_table_sp, BorderLayout.CENTER);
			
		                                
		                                //Code for logo
                                                     Logo = new JLabel();
                                                     Logo.setIcon(new ImageIcon(this.getClass().getResource("/img/vlc1.PNG"))); 
                                           	        
                                                     Logo.setBounds(1098, 314, 202, 82);
                                             
                                                             frame.getContentPane().add(Logo);
                                                             
                                                             
	    } 
	      
	    public static void main(String[] args) { 
	        new Appointment(); 
	        
	        AppointmentData    Appointment1 = new AppointmentData( "Rizwan", "Nile",   "Male",   "03216350058", "nileriz@gmail.com", "Mon May 22 00:19:50 PKT 2019", "Monday Evening",   "05:00:00", "05:30:00", "badminton");
	        AppointmentData    Appointment2 = new AppointmentData( "amer",   "hussam", "Male",   "03235147889", "hussam@gmail.com",  "Mon May 29 00:20:51 PKT 2019", "Monday Evening",   "05:00:00", "05:30:00", "Swimmimg");
	        AppointmentData    Appointment3 = new AppointmentData( "khan",   "Amir",   "Male",   "03215478898", "amir@gmail.com",    "Mon May 05 00:19:50 PKT 2019",  "Saturday Morning", "12:30:00", "01:00:00", "gym");
	        AppointmentData    Appointment4 = new AppointmentData( "ahmed",  "nawal",  "Female", "03214780025", "nawal@gmail.com",   "Mon May 12 00:19:50 PKT 2019", "Monday Evening",   "05:00:00", "05:30:00", "gym");
	        AppointmentData    Appointment5 = new AppointmentData( "ahsen",  "hifza",  "Male",   "03216365478", "hifza@gmail.com",   "Mon May 19 00:19:50 PKT 2019", "Monday Evening",   "06:00:00", "06:30:00", "Swimmimg");
	        
	        
	        ArrayList <AppointmentData> list =  Singleton.getInstance().getappointmentdataList();			
		    
		    list.add(Appointment1);
		    list.add(Appointment2);
		    list.add(Appointment3);
		    list.add(Appointment4);
		    list.add(Appointment5);
	    } 
	} 