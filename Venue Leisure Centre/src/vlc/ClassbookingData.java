package vlc;

public class ClassbookingData {
	String Student_id_tf, Coach_id_tf, First_name_tf, Last_name_tf, Class_cb, Room_cb, date_dc, Time_tc, Time_cb;

	public ClassbookingData(String Student_id_tf, String Coach_id_tf, String First_name_tf, String Last_name_tf, String Class_cb, String Room_cb, String date_dc, String Time_tc, String Time_cb) {
		super();
		this.Student_id_tf = Student_id_tf;
		this.Coach_id_tf =Coach_id_tf;
		this.First_name_tf = First_name_tf;
		this.Last_name_tf = Last_name_tf;
		this.Class_cb = Class_cb;
		this.Room_cb = Room_cb;
		this.date_dc = date_dc;
		this.Time_tc = Time_tc;
		this.Time_cb = Time_cb;
	}

	public String getStudent_id() {
		return Student_id_tf;
	}

	public void setStudent_id(String Student_id_tf) {
		this.Student_id_tf = Student_id_tf;
	}
	
	
	public String getCoach_id() {
		return Coach_id_tf;
	}

	public void setCoach_id(String Coach_id_tf) {
		this.Coach_id_tf = Coach_id_tf;
	}
	
	public String getFirst() {
		return First_name_tf;
	}

	public void setFirst(String First_name_tf) {
		this.First_name_tf = First_name_tf;
	}
	
	public String getLast() {
		return Last_name_tf;
	}

	public void setLast(String Last_name_tf) {
		this.Last_name_tf = Last_name_tf;
	}

	public String getClass_cb() {
		return Class_cb;
	}

	public void setClass_cb(String Class_cb) {
		this.Class_cb = Class_cb;
	}

	public String getRoom_cb() {
		return Room_cb;
	}

	public void setRoom_cb(String Room_cb) {
		this.Room_cb = Room_cb;
	}
	
	public String getDate_dc() {
		return date_dc;
	}

	public void setDate_dc(String date_dc) {
		this.date_dc = date_dc;
	}

	public String getTime_tc() {
		return Time_tc;
	}

	public void setTime_tc(String Time_tc) {
		this.Time_tc = Time_tc;
	}
	
	public String getTime_cb() {
		return Time_cb;
	}

	public void setSTime_cb(String Time_cb) {
		this.Time_cb = Time_cb;
	}
	
}


