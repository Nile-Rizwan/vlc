package vlc;

public class StudentregistrationData {


	String Student_id_tf, First_name_tf, Last_name_tf, Guardians_first_name_tf, Guardians_last_name_tf, Date_of_birth_dc, Date_of_admission_dc, Gender_bg, Blood_group_cb, Email_tf, Telephone_no_tf, Emergency_contact_tf, Street_no_tf, House_no_tf, Zip_code_tf, Class_to_attend_cb, Alloted_room_cb;

	public StudentregistrationData(String Student_id_tf, String First_name_tf, String Last_name_tf, String Guardians_first_name_tf, String Guardians_last_name_tf, String Date_of_birth_dc, String Date_of_admission_dc, String Gender_bg, String Blood_group_cb, String Email_tf, String Telephone_no_tf, String Emergency_contact_tf, String Street_no_tf, String House_no_tf, String Zip_code_tf, String Class_to_attend_cb, String Alloted_room_cb) {
		super();
		this.Student_id_tf = Student_id_tf;
		this.First_name_tf = First_name_tf;
		this.Last_name_tf = Last_name_tf;
		this.Guardians_first_name_tf = Guardians_first_name_tf;
		this.Guardians_last_name_tf = Guardians_last_name_tf;
		this.Date_of_birth_dc = Date_of_birth_dc;
		this.Date_of_admission_dc = Date_of_admission_dc;
		this.Gender_bg = Gender_bg;
		this.Blood_group_cb = Blood_group_cb;
		this.Email_tf = Email_tf;
		this.Telephone_no_tf = Telephone_no_tf;
		this.Emergency_contact_tf = Emergency_contact_tf;
		this.Street_no_tf = Street_no_tf;
		this.House_no_tf = House_no_tf;
		this.Zip_code_tf = Zip_code_tf;
		this.Class_to_attend_cb = Class_to_attend_cb;
		this.Alloted_room_cb = Alloted_room_cb;
	}

	public String getStudent_id() {
		return Student_id_tf;
	}

	public void setStudent_id(String Student_id_tf) {
		this.Student_id_tf = Student_id_tf;
	}

	public String getFname() {
		return First_name_tf;
	}

	public void setFname(String First_name_tf) {
		this.First_name_tf = First_name_tf;
	}
	
	public String getLname() {
		return Last_name_tf;
	}

	public void setLname(String Last_name_tf) {
		this.Last_name_tf = Last_name_tf;
	}
	
	public String getGuardians_First() {
		return Guardians_first_name_tf;
	}

	public void setGuardians_First(String Guardians_first_name_tf) {
		this.Guardians_first_name_tf = Guardians_first_name_tf;
	}
	
	public String getGuardians_Last() {
		return Guardians_last_name_tf;
	}

	public void setGuardians_Last(String Guardians_last_name_tf) {
		this.Guardians_last_name_tf = Guardians_last_name_tf;
	}
	
	public String getDate_of_birth_dc() {
		return Date_of_birth_dc;
	}

	public void setDate_of_birth_dc(String Date_of_birth_dc) {
		this.Date_of_birth_dc = Date_of_birth_dc;
	}

	public String getDate_of_admission_dc() {
		return Date_of_admission_dc;
	}

	public void setDate_of_admission_dc(String Date_of_admission_dc) {
		this.Date_of_admission_dc = Date_of_admission_dc;
	}

	public String getGender_bg () {
		return Gender_bg ;
	}

	public void setGender_bg (String Gender_bg ) {
		this.Gender_bg  = Gender_bg ;
	}

	public String getBlood_group_cb() {
		return Blood_group_cb;
	}

	public void setBlood_group_cb(String Blood_group_cb) {
		this.Blood_group_cb = Blood_group_cb;
	}

	public String getEmail() {
		return Email_tf;
	}

	public void setEmail(String Email_tf) {
		this.Email_tf = Email_tf;
	}

	public String getTelephone_no() {
		return Telephone_no_tf;
	}

	public void setTelephone_no(String Telephone_no_tf) {
		this.Telephone_no_tf = Telephone_no_tf;
	}

	public String getEmergency_contact() {
		return Emergency_contact_tf;
	}

	public void setEmergency_contact(String Emergency_contact_tf) {
		this.Emergency_contact_tf = Emergency_contact_tf;
	}

	public String getStreet_no() {
		return Street_no_tf;
	}

	public void setStreet_no(String Street_no_tf) {
		this.Street_no_tf = Street_no_tf;
	}

	public String getHouse_no() {
		return House_no_tf;
	}

	public void setHouse_no(String House_no_tf) {
		this.House_no_tf = House_no_tf;
	}

	public String getZip_code() {
		return Zip_code_tf;
	}

	public void setZip_code(String Zip_code_tf) {
		this.Zip_code_tf = Zip_code_tf;
	}
	
	
	
	
	
	
	
	public String getClass_to_attend_cb() {
		return Class_to_attend_cb;
	}

	public void setClass_to_attend_cb(String Class_to_attend_cb) {
		this.Class_to_attend_cb = Class_to_attend_cb;
	}
	
	
	
	
	
	
	
	
	
	public String getAlloted_room_cb() {
		return Alloted_room_cb;
	}

	public void setAlloted_room_cb(String Alloted_room_cb) {
		this.Alloted_room_cb = Alloted_room_cb;
	}

}
