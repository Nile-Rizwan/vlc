package vlc;

public class CoachregistrationData {

	String Coach_id_tf, First_name_tf, Last_name_tf, Father_name_tf, Date_of_birth_dc, Date_of_hire_dc, Gender_bg, Blood_group_cb, Basic_medical_tranning_bg, Icf_certified_bg, Certifications_aquired_tf, Educational_background_tf, Email_tf, Telephone_no_tf, Emergency_contact_tf, Street_no_tf, House_no_tf, Zip_code_tf, Coaches_salary_tf, Previous_experience_cb, Class_to_teach_cb, Alloted_room_cb;

	public CoachregistrationData(String Coach_id_tf, String First_name_tf, String Last_name_tf, String Father_name_tf, String Date_of_birth_dc, String Date_of_hire_dc, String Gender_bg, String Blood_group_cb, String Basic_medical_tranning_bg , String Icf_certified_bg, String Certifications_aquired_tf, String Educational_background_tf, String Email_tf, String Telephone_no_tf, String Emergency_contact_tf, String Street_no_tf, String House_no_tf, String Zip_code_tf, String Coaches_salary_tf, String Previous_experience_cb, String Class_to_teach_cb, String Alloted_room_cb) {
		super();
		this.Coach_id_tf = Coach_id_tf;
		this.First_name_tf = First_name_tf;
		this.Last_name_tf = Last_name_tf;
		this.Father_name_tf = Father_name_tf;
		this.Date_of_birth_dc = Date_of_birth_dc;
		this.Date_of_hire_dc = Date_of_hire_dc;
		this.Gender_bg = Gender_bg;
		this.Blood_group_cb = Blood_group_cb;
		this.Basic_medical_tranning_bg = Basic_medical_tranning_bg;
		this.Icf_certified_bg = Icf_certified_bg;
		this.Certifications_aquired_tf = Certifications_aquired_tf;
		this.Educational_background_tf = Educational_background_tf;
		this.Email_tf = Email_tf;
		this.Telephone_no_tf = Telephone_no_tf;
		this.Emergency_contact_tf = Emergency_contact_tf;
		this.Street_no_tf = Street_no_tf;
		this.House_no_tf = House_no_tf;
		this.Zip_code_tf = Zip_code_tf;
		this.Coaches_salary_tf = Coaches_salary_tf;
		this.Previous_experience_cb = Previous_experience_cb;
		this.Class_to_teach_cb = Class_to_teach_cb;
		this.Alloted_room_cb = Alloted_room_cb;
	}

	public String getCoach_id() {
		return Coach_id_tf;
	}

	public void setCoach_id(String Coach_id_tf) {
		this.Coach_id_tf = Coach_id_tf;
	}
	
	public String getFirst_name() {
		return First_name_tf;
	}

	public void setFirst_name(String First_name_tf) {
		this.First_name_tf = First_name_tf;
	}
	
	public String getLast_name() {
		return Last_name_tf;
	}

	public void setLast_name(String Last_name_tf) {
		this.Last_name_tf = Last_name_tf;
	}
	
	
	public String getFather_name() {
		return Father_name_tf;
	}

	public void setFather_name(String Father_name_tf) {
		this.Father_name_tf = Father_name_tf;
	}
	
	public String getDate_of_birth_dc() {
		return Date_of_birth_dc;
	}

	public void setDate_of_birth_dc(String Date_of_birth_dc) {
		this.Date_of_birth_dc = Date_of_birth_dc;
	}
	
	
	
	
	
	
	
	
	public String getDate_of_hire_dc() {
		return Date_of_hire_dc;
	}

	public void setDate_of_hire_dc(String Date_of_hire_dc) {
		this.Date_of_hire_dc = Date_of_hire_dc;
	}
	
	
	
	
	
	
	
	public String getGender_bg() {
		return Gender_bg;
	}

	public void setGender_bg(String Gender_bg) {
		this.Gender_bg = Gender_bg;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String getBlood_group_cb() {
		return Blood_group_cb;
	}

	public void setBlood_group_cb(String Blood_group_cb) {
		this.Blood_group_cb = Blood_group_cb;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public String getBasic_medical_tranning_bg() {
		return Basic_medical_tranning_bg;
	}

	public void setBasic_medical_tranning_bg(String Basic_medical_tranning_bg) {
		this.Basic_medical_tranning_bg = Basic_medical_tranning_bg;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public String getIcf_certified_bg() {
		return Icf_certified_bg;
	}

	public void setIcf_certified_bg(String Icf_certified_bg) {
		this.Icf_certified_bg = Icf_certified_bg;
	}
	
	
	
	
	
	
	
	
	
	
	public String getCertifications_aquired() {
		return Certifications_aquired_tf;
	}

	public void setCertifications_aquired(String Certifications_aquired_tf) {
		this.Certifications_aquired_tf = Certifications_aquired_tf;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public String getEducational_background() {
		return Educational_background_tf;
	}

	public void setEducational_background(String Educational_background_tf) {
		this.Educational_background_tf = Educational_background_tf;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String getEmail() {
		return Email_tf;
	}

	public void setEmail(String Email_tf) {
		this.Email_tf = Email_tf;
	}
	
	
	
	
	
	
	
	
	
	
	public String getTelephone_no() {
		return Telephone_no_tf;
	}

	public void setTelephone_no(String Telephone_no_tf) {
		this.Telephone_no_tf = Telephone_no_tf;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String getEmergency_contact() {
		return Emergency_contact_tf;
	}

	public void setEmergency_contact(String Emergency_contact_tf) {
		this.Emergency_contact_tf = Emergency_contact_tf;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public String getStreet_no() {
		return Street_no_tf;
	}

	public void setStreet_no(String Street_no_tf) {
		this.Street_no_tf = Street_no_tf;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public String getHouse_no() {
		return House_no_tf;
	}

	public void setHouse_no(String House_no_tf) {
		this.House_no_tf = House_no_tf;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String getZip_code() {
		return Zip_code_tf;
	}

	public void setZip_code(String Zip_code_tf) {
		this.Zip_code_tf = Zip_code_tf;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public String getCoaches_salary() {
		return Coaches_salary_tf;
	}

	public void setCoaches_salary(String Coaches_salary_tf) {
		this.Coaches_salary_tf = Coaches_salary_tf;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public String getPrevious_experience_cb() {
		return Previous_experience_cb;
	}

	public void setPrevious_experience_cb(String Previous_experience_cb) {
		this.Previous_experience_cb = Previous_experience_cb;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public String getClass_to_teach_cb() {
		return Class_to_teach_cb;
	}

	public void setClass_to_teach_cb(String Class_to_teach_cb) {
		this.Class_to_teach_cb = Class_to_teach_cb;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public String getAlloted_room_cb() {
		return Alloted_room_cb;
	}

	public void setAlloted_room_cb(String Alloted_room_cb) {
		this.Alloted_room_cb = Alloted_room_cb;
	}
	
}
