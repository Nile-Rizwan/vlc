package vlc;

import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.*;


public class Mainpage  {
	
	JFrame frame; 
	JLabel Venue_leisure_centre;      
	JButton Student_registration_btn;       
	JButton Coach_registration_btn;     
	JButton Records_btn; 
	JButton Appointment_btn; 
	JButton Fee_billing_btn; 
	JButton Class_booking_btn;
	JButton Attendence_btn;
	Container c;
	JLabel Logo;
	
      
    Mainpage() 
    { 
		
         
        frame=new JFrame("Venue Leisure Centre"); 
        Image img = Toolkit.getDefaultToolkit().getImage("Resources/img/vlcmin.PNG");
        frame.setIconImage(img);
        c = frame.getContentPane();
		c.setLayout(new BorderLayout());
    	JPanel centerpanel = new JPanel(new GridLayout(3, 3));
    	JPanel northpanel = new JPanel(new FlowLayout());
    	JPanel southpanel = new JPanel(new FlowLayout());
    	JPanel eastpanel = new JPanel(new FlowLayout());
    	JPanel westpanel = new JPanel(new FlowLayout());

        Venue_leisure_centre = new JLabel("Venue Leisure Centre"); 

        Student_registration_btn = new JButton("Student Registration"); 
        Coach_registration_btn = new JButton("Coach Registration"); 
        Records_btn = new JButton("Records"); 
        Appointment_btn = new JButton("Appointment"); 
        Fee_billing_btn = new JButton("Fee Billing"); 
        Class_booking_btn = new JButton("Class Booking");
        Attendence_btn = new JButton ("Attendence");
        
        Logo = new JLabel();
        Logo.setIcon(new ImageIcon(this.getClass().getResource("/img/vlc.PNG")));
        
       

        northpanel.add(Venue_leisure_centre);
        
		centerpanel.add(Student_registration_btn);       
        centerpanel.add(Coach_registration_btn);     
        centerpanel.add(Records_btn); 
        centerpanel.add(Appointment_btn); 
        centerpanel.add(Fee_billing_btn); 
        centerpanel.add(Class_booking_btn);
        centerpanel.add(Attendence_btn);
        eastpanel.add(Logo);

        c.add(northpanel, BorderLayout.NORTH);
        c.add(centerpanel, BorderLayout.CENTER);
        c.add(southpanel, BorderLayout.SOUTH);
        c.add(eastpanel, BorderLayout.EAST);
        c.add(westpanel, BorderLayout.WEST);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        
        frame.setSize(1013, 312); 
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
          
        frame.setVisible(true);
        
        Student_registration_btn.addActionListener(new ActionListener() {
 	       public void actionPerformed(ActionEvent e) {
 	    	  Studentregistration.main(null);
		       frame.dispose();
 	}
 });
        Student_registration_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/student reg.png"))); 
                             
        
        Coach_registration_btn.addActionListener(new ActionListener() {
 	      public void actionPerformed(ActionEvent e) {
 	    	 Coachregistration.main(null);
		      frame.dispose();
 	}
 });
        Coach_registration_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/coach reg.png"))); 
        
        
        Records_btn.addActionListener(new ActionListener() {
 	       public void actionPerformed(ActionEvent e) {
 	    	  Report.main(null);
		   frame.dispose();
 	}
 });
        Records_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/records.png"))); 
        
        
        Appointment_btn.addActionListener(new ActionListener() {
 	       public void actionPerformed(ActionEvent e) {
 	    	  Appointment.main(null);
		   frame.dispose();
 	}
 });    
        Appointment_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/appointment.png"))); 
                  
        
        Fee_billing_btn.addActionListener(new ActionListener() {
 	      public void actionPerformed(ActionEvent e) {
 	    	 Feebilling.main(null);
		  frame.dispose();
 	}
 });
        Fee_billing_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/fee billing.png"))); 
        
               
        Class_booking_btn.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
        	  Classbooking.main(null);
          frame.dispose();
     }
  });   
        Class_booking_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/class booking.png")));
        
        
        Attendence_btn.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		StudentAttendance.main(null);
                frame.dispose();
        	}
        });
        
        Attendence_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/Attendance.png")));
      
    		   
           
    } 
      
    public static void main(String[] args) { 
        new Mainpage(); 
        
    } 
} 