package vlc;

public class AttendanceData {
	String  Coach_name_tf, Student_name_tf, Class_cb, Room_cb, date_dc, Time_tc, Time_cb, Attendance_bg;

	public AttendanceData(String Coach_name_tf, String Student_name_tf, String Class_cb, String Room_cb, String date_dc, String Time_tc, String Time_cb, String Attendance_bg) {
		super();
		this.Coach_name_tf =  Coach_name_tf;
		this.Student_name_tf = Student_name_tf;
		this.Class_cb = Class_cb;
		this.Room_cb = Room_cb;
		this.date_dc = date_dc;
		this.Time_tc = Time_tc;
		this.Time_cb = Time_cb;
		this.Attendance_bg = Attendance_bg;
	}

	public String getCoach_name_tf() {
		return Coach_name_tf;
	}

	public void setCoach_name_tf(String Coach_name_tf) {
		this.Coach_name_tf = Coach_name_tf;
	}

	public String getStudent_name_tf() {
		return Student_name_tf;
	}

	public void setStudent_name_tf(String Student_name_tf) {
		this.Student_name_tf = Student_name_tf;
	}

	public String getClass_cb() {
		return Class_cb;
	}

	public void setClass_cb(String Class_cb) {
		this.Class_cb = Class_cb;
	}

	public String getRoom_cb() {
		return Room_cb;
	}

	public void setRoom_cb(String Room_cb) {
		this.Room_cb = Room_cb;
	}
	
	public String getDate_dc() {
		return date_dc;
	}

	public void setDate_dc(String date_dc) {
		this.date_dc = date_dc;
	}

	public String getTime_tc() {
		return Time_tc;
	}

	public void setTime_tc(String Time_tc) {
		this.Time_tc = Time_tc;
	}
	
	public String getTime_cb() {
		return Time_cb;
	}

	public void setSTime_cb(String Time_cb) {
		this.Time_cb = Time_cb;
	}
	
	public String getAttendance_bg() {
		return Attendance_bg;
	}

	public void setAttendance_bg(String Attendance_bg) {
		this.Attendance_bg = Attendance_bg;
	}
	
}


