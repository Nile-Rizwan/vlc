package vlc;

public class AppointmentData {

	String  Parent_name_tf, Student_name_tf, Gender_bg, Telephone_no_tf, Email_tf, Date_of_appointment_dc, Day_of_appointment_cb, Time_from_tc, Time_to_tc, Coach_to_meet_cb;

	public AppointmentData(String Parent_name_tf, String Student_name_tf, String Gender_bg, String Telephone_no_tf, String Email_tf, String Date_of_appointment_dc, String Day_of_appointment_cb, String Time_from_tc, String Time_to_tc, String Coach_to_meet_cb) {
		super();
		
		this.Parent_name_tf = Parent_name_tf;
		this.Student_name_tf = Student_name_tf;
		this.Gender_bg = Gender_bg;
		this.Telephone_no_tf = Telephone_no_tf;
		this.Email_tf = Email_tf;
		this.Date_of_appointment_dc = Date_of_appointment_dc;
		this.Day_of_appointment_cb = Day_of_appointment_cb;
		this.Time_from_tc = Time_from_tc;
		this.Time_to_tc = Time_to_tc;
		this.Coach_to_meet_cb = Coach_to_meet_cb;
		
	}



	public String getParent_name_tf() {
		return Parent_name_tf;
	}

	public void setParent_name_tf(String Parent_name_tf) {
		this.Parent_name_tf = Parent_name_tf;
	}

	public String getStudent_name_tf() {
		return Student_name_tf;
	}

	public void setStudent_name_tf(String Student_name_tf) {
		this.Student_name_tf = Student_name_tf;
	}
	
	public String getGender_bg() {
		return Gender_bg;
	}

	public void setGender_bg(String Gender_bg) {
		this.Gender_bg = Gender_bg;
	}
	
	
	public String getTelephone() {
		return Telephone_no_tf;
	}

	public void setTelephone(String Telephone_no_tf) {
		this.Telephone_no_tf = Telephone_no_tf;
	}
	
	public String getEmail() {
		return Email_tf;
	}

	public void setEmail(String Email_tf) {
		this.Email_tf = Email_tf;
	}
	
	public String getAppointment_date_dc() {
		return Date_of_appointment_dc;
	}

	public void setAppointment_date_dc(String Date_of_appointment_dc) {
		this.Date_of_appointment_dc = Date_of_appointment_dc;
	}

	public String getDay_of_appointment_cb() {
		return Day_of_appointment_cb;
	}

	public void setDay_of_appointment_cb(String Day_of_appointment_cb) {
		this.Day_of_appointment_cb = Day_of_appointment_cb;
	}

	public String getTime_from_tc() {
		return Time_from_tc;
	}

	public void setTime_from_tc(String Time_from_tc) {
		this.Time_from_tc = Time_from_tc;
	}

	public String getTime_to_tc() {
		return Time_to_tc;
	}

	public void setTime_to_tc(String Time_to_tc) {
		this.Time_to_tc = Time_to_tc;
	}
	

	
	public String getCoach_meet_cb() {
		return Coach_to_meet_cb;
	}

	public void setCoach_meet_cb(String Coach_to_meet_cb) {
		this.Coach_to_meet_cb = Coach_to_meet_cb;
	}
	
	
	
}
