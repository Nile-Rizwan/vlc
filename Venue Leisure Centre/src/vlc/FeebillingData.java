package vlc;

public class FeebillingData {



	String Student_id_tf, First_name_tf, Last_name_tf, Payment_type_bg, Card_number_tf, Cvv_code_tf, Credit_card_type_cb, Valid_from_dc, Valid_to_dc, Payment_date_dc, Payment_time_tc, Payment_time_cb, Fees_paid_tf, Fees_due_tf;

	public FeebillingData(String Student_id_tf, String First_name_tf, String Last_name_tf, String Payment_type_bg, String Card_number_tf, String Cvv_code_tf, String Credit_card_type_cb, String Valid_from_dc, String Valid_to_dc, String Payment_date_dc, String Payment_time_tc, String Payment_time_cb, String Fees_paid_tf, String Fees_due_tf) {
		super();
		this.Student_id_tf = Student_id_tf;
		this.First_name_tf = First_name_tf;
		this.Last_name_tf = Last_name_tf;
		this.Payment_type_bg = Payment_type_bg;
		this.Card_number_tf = Card_number_tf;
		this.Cvv_code_tf = Cvv_code_tf;
		this.Credit_card_type_cb = Credit_card_type_cb;
		this.Valid_from_dc = Valid_from_dc; 
		this.Valid_to_dc = Valid_to_dc;
		this.Payment_date_dc = Payment_date_dc;
		this.Payment_time_tc = Payment_time_tc;
		this.Payment_time_cb =Payment_time_cb;
		this.Fees_paid_tf = Fees_paid_tf;
		this.Fees_due_tf = Fees_due_tf;
	}

	public String getStudent_id() {
		return Student_id_tf;
	}

	public void setStudent_id(String Student_id_tf) {
		this.Student_id_tf = Student_id_tf;
	}

	public String getFname() {
		return First_name_tf;
	}

	public void setFname(String First_name_tf) {
		this.First_name_tf = First_name_tf;
	}

	public String getLname() {
		return Last_name_tf;
	}

	public void setLname(String Last_name_tf) {
		this.Last_name_tf = Last_name_tf;
	}

	public String getPayment_bg() {
		return Payment_type_bg;
	}

	public void setPayment_bg(String Payment_type_bg) {
		this.Payment_type_bg = Payment_type_bg;
	}
	
	public String getCardnum() {
		return Card_number_tf;
	}

	public void setCardnum(String Card_number_tf) {
		this.Card_number_tf = Card_number_tf;
	}

	public String getCvv() {
		return Cvv_code_tf;
	}

	public void setCvv(String Cvv_code_tf) {
		this.Cvv_code_tf = Cvv_code_tf;
	}

	public String getCreditcard_cb() {
		return Credit_card_type_cb;
	}

	public void setCreditcard_cb(String Credit_card_type_cb) {
		this.Credit_card_type_cb = Credit_card_type_cb;
	}
	
	public String getValidfrom_dc() {
		return Valid_from_dc;
	}

	public void setValidfrom_dc(String Valid_from_dc) {
		this.Valid_from_dc = Valid_from_dc;
	}

	public String getValidto_dc() {
		return Valid_to_dc;
	}

	public void setValidto_dc(String Valid_to_dc) {
		this.Valid_to_dc = Valid_to_dc;
	}

	public String getPaymentdate_dc() {
		return Payment_date_dc;
	}

	public void setPaymentdate_dc(String Payment_date_dc) {
		this.Payment_date_dc = Payment_date_dc;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public String getPaymenttime_tc() {
		return Payment_time_tc;
	}

	public void setPaymenttime_tc(String Payment_time_tc) {
		this.Payment_time_tc = Payment_time_tc;
	}

	public String getPaymenttime_cb() {
		return Payment_time_cb;
	}

	public void setPaymenttime_cb(String Payment_time_cb) {
		this.Payment_time_cb = Payment_time_cb;
	}

	public String getFeespaid() {
		return Fees_paid_tf;
	}

	public void setFeespaid(String Fees_paid_tf) {
		this.Fees_paid_tf = Fees_paid_tf;
	}

	public String getFeesdue() {
		return Fees_due_tf;
	}

	public void setFeesdue(String Fees_due_tf) {
		this.Fees_due_tf = Fees_due_tf;
	}
}


