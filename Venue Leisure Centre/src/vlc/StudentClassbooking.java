package vlc;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import com.toedter.calendar.JDateChooser;

import lu.tudor.santec.jtimechooser.JTimeChooser;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.*;
import java.util.ArrayList;


public class StudentClassbooking {
	 JFrame frame; 
	 JMenuBar menubar6;
	 JMenu filemenu;  JMenuItem Exit;
	 JMenu Logoutmenu; JMenuItem Logout_mi;
	 JMenu Helpmenu; JMenuItem Help_mi;
	 JLabel Coach_lookup;
	 JLabel  Student_id;        JTextField  Student_id_tf;
	 JLabel  Coach_id;          JTextField  Coach_id_tf;
	 JLabel First_name;         JTextField First_name_tf;
	 JLabel Last_name;          JTextField Last_name_tf;
	 JLabel Class;              JComboBox Class_cb;
	 JLabel Room;               JComboBox Room_cb;
	 JLabel Date;               JDateChooser date_dc;
	 JLabel Time;               JTimeChooser Time_tc;      JComboBox Time_cb;
	 JButton Load_data_btn;
	 JTable Class_booking_table;
	 JScrollPane Class_booking_table_sp;
	 JButton Save_btn;
	 JButton Clear_btn;
	 JButton Exit_btn;
	 JLabel Search;
	 JTextField Search_tf;
	 
	 JLabel Logo;
	 DefaultTableModel model;
	 


	 StudentClassbooking() 
	    { 
	         
	        frame=new JFrame("Venue Leisure Centre"); 
	        Image img = Toolkit.getDefaultToolkit().getImage("Resources/img/vlcmin.PNG");
	        frame.setIconImage(img);
	        
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
	        
	        frame.setSize(1235, 441); 
	        
	        frame.setLocationRelativeTo(null);
	          
	        frame.getContentPane().setLayout(null);   
	          
	        frame.setVisible(true);
	        
	        menubar6 = new JMenuBar ();
	        frame.setJMenuBar (menubar6);
	        
	        filemenu = new JMenu ("File");
	        menubar6.add (filemenu);
	        
	       
	        
	      
	     

	        
	        filemenu.add(new JSeparator());
	        
	        Exit = new JMenuItem("Exit");
	        Exit.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
			             int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
			             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

			                    {
			                     System.exit(0);
			                    }
	        	}
	        });
	        filemenu.add(Exit);
	        Exit.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png")));
	        
	        Logoutmenu = new JMenu ("Logout");
	        menubar6.add (Logoutmenu);
	         
	        
	        Logout_mi = new JMenuItem("Logout");
	        Logout_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
	        		int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to logout?","Logout",JOptionPane.YES_NO_OPTION);
		             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 
		                    {
		            	 Loginselection.main(null);
			                 frame.dispose();
		                    }
	        	}
	        });
	        Logoutmenu.add(Logout_mi);
	        Logout_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/logout.png"))); 
	        
	        Helpmenu = new JMenu ("Help");
	        menubar6.add (Helpmenu);
	         
	        
	        Help_mi = new JMenuItem("Help");
	        Help_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Help.main(null); 
	        	}
	        });
	        
	        Helpmenu.add(Help_mi);
	        Help_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/help.png")));
	        
	        
            Coach_lookup = new JLabel("Class Booking"); 
	        
                        Coach_lookup.setBounds(418, 13, 126, 26); 
	          
	                                frame.getContentPane().add(Coach_lookup); 
	       
            Student_id = new JLabel("Student ID"); 
	        
	                  Student_id.setBounds(24, 55, 75, 26); 
	        	          
	        	                 frame.getContentPane().add(Student_id);
	        	
	        Student_id_tf = new JTextField(); 
	        	        
	        	         Student_id_tf.setBounds(125, 57, 210, 22);
	        	          
	        	                       frame.getContentPane().add(Student_id_tf);
	        	                       
	        	                       Coach_id = new JLabel("Coach ID"); 
	        	           	        
	        	                       Coach_id.setBounds(24, 100, 75, 26); 
	        	   	        	          
	        	   	        	            frame.getContentPane().add( Coach_id);
	        	   	        	
	        	   	        Coach_id_tf = new JTextField(); 
	        	   	        	        
	        	   	        	       Coach_id_tf.setBounds(125, 102, 210, 22);
	        	   	        	          
	        	   	        	                   frame.getContentPane().add( Coach_id_tf);
	        	   	        	                   
	        	   	        	                First_name = new JLabel("First Name"); 
	        	   	        	 	        
	        	   	               		  First_name.setBounds(24, 139, 75, 26); 
	        	   	   	          
	        	   	               		  			 frame.getContentPane().add(First_name);
	        	   	   	
	        	   	               First_name_tf = new JTextField(); 
	        	   	   	        
	        	   	                            First_name_tf.setBounds(125, 141, 210, 22);
	        	   	   	          
	        	   	   	                                   frame.getContentPane().add(First_name_tf);

	        	   	               Last_name = new JLabel("Last Name"); 
	        	   	   	        
	        	   	                        Last_name.setBounds(24, 178, 75, 26); 
	        	   	   	          
	        	   	   	                          frame.getContentPane().add(Last_name);
	        	   	   	
	        	   	               Last_name_tf = new JTextField(); 
	        	   	   	        
	        	   	                           Last_name_tf.setBounds(125, 180, 210, 22);
	        	   	   	          
	        	   	   	                                 frame.getContentPane().add(Last_name_tf);

	        Class = new JLabel("Class");
	        	        
	        	 Class.setBounds(24, 219, 97, 26);
	        	        	        
	        	       frame.getContentPane().add(Class);
	        	                    
	        Class_cb = new JComboBox();
	        	        	        
	        	    Class_cb.setModel(new DefaultComboBoxModel(new String[] {"Swimming", "badminton", "gym"}));
	        	        	        
	        	    Class_cb.setBounds(125, 219, 87, 26);
	        	        			
	        	        	 frame.getContentPane().add(Class_cb);
	        	        	 
	        	        	 
	        	        	 Room = new JLabel("Alloted Room");
	        	 	        
	        	        	 Room.setBounds(24, 254, 97, 26);
	 	        
	                                      frame.getContentPane().add(Room);
	             
	                                      Room_cb = new JComboBox();
	 	        
	                                      Room_cb.setModel(new DefaultComboBoxModel(new String[] {"Studio A", "Studio B", "Studio C", "Pool", "Gym"}));
	 	        
	                                      Room_cb.setBounds(125, 256, 87, 28);
	 			
	 	                                       frame.getContentPane().add(Room_cb);
	        	        
	        
	        	        
	        Date = new JLabel("Date"); 
	        	        	        	        
	        	Date.setBounds(32, 291, 87, 26); 
	        	        	        	        	          
	        	     frame.getContentPane().add(Date);
	        	        	        	        	        
	        date_dc = new JDateChooser();
	        
	        	   date_dc.setBounds(125, 297, 200, 20);
	        	   
	        	   
   	        	           frame.getContentPane().add(date_dc);
	        	        	        	              
	        Time = new JLabel("Time"); 
	        	        	        	        	        
	        	Time.setBounds(32, 323, 42, 26); 
	        	        	        	        	        	          
	        	     frame.getContentPane().add(Time);
	        	        	        	        	        	        
	        Time_tc = new JTimeChooser();
	        	   Time_tc.setBounds(125, 325, 118, 22);
	        	          frame.getContentPane().add(Time_tc);
	        Time_cb = new JComboBox();
	        	     	        	        	        	        	        
	        	   Time_cb.setModel(new DefaultComboBoxModel(new String[] {"am", "pm"}));
	        	     	        	        	        	        	        
	        	   Time_cb.setBounds(271, 324, 64, 26);
	        	     	        	        	        	        			
	        	     	   frame.getContentPane().add(Time_cb);
	        	     	   
	        Load_data_btn = new JButton("Load Data"); 
	        Load_data_btn.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
	        		ArrayList<ClassbookingData> vector = Singleton.getInstance().getclassbookingdataList();
	   		            String[] columnNames = { "Student id", "Coach id", "First Name", "Last Name", "Class", "Room", "Date", "Time", "am/pm"}; 

	   		  	 Object[][] data;
	   		  	 
				data = new Object[vector.size()][10];

				for (int i = 0; i < data.length; i++) {
						ClassbookingData a = vector.get(i);
					data[i][0] = a.getStudent_id();
					data[i][1] = a.getCoach_id();
					data[i][2] = a.getFirst();
					data[i][3] = a.getLast();
					data[i][4] = a.getClass_cb();
					data[i][5] = a.getRoom_cb();
					data[i][6] = a.getDate_dc();
					data[i][7] = a.getTime_tc();
					data[i][8] = a.getTime_cb();
					
			
					
					
				}
	   				model = new DefaultTableModel(data, columnNames);
	   				Class_booking_table = new JTable(model);
	   				Class_booking_table_sp = new JScrollPane(Class_booking_table);

	   				Class_booking_table_sp.setVisible(true);

	   				Class_booking_table_sp.setBounds(347, 55, 650, 295);

	                     frame.getContentPane().add(Class_booking_table_sp);	
	                  
	                 }
	                 
              });
	        	
	        
	        Load_data_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/load data.png"))); 
  	                    
	                     Load_data_btn.setBounds(1009, 55, 124, 33);
		        	        	        	                    	   	          
		                               frame.getContentPane().add(Load_data_btn);
		                               
		                               Save_btn = new JButton("Save"); 
		                               Save_btn.addActionListener(new ActionListener() {
		                               	public void actionPerformed(ActionEvent e) {
		                               		ArrayList<ClassbookingData> al = Singleton.getInstance().getclassbookingdataList();
		                               		ClassbookingData cbd = new ClassbookingData(Student_id_tf.getText(), Coach_id_tf.getText(), First_name_tf.getText(), Last_name_tf.getText(), Class_cb.getSelectedItem().toString(), Room_cb.getSelectedItem().toString(), date_dc.getDate().toString(), Time_tc.getTimeField().getText(), Time_cb.getSelectedItem().toString() );
		   		                	 
       		   		                	 al.add(cbd);
       		   		                	JOptionPane.showMessageDialog(null, "Data has been successfully inserted");        
       		   		                 }
       		   	                });
		                               	
		                               
		                               Save_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/save.png")));
			 	 	   			        
			 	                         Save_btn.setBounds(1009, 97, 124, 33);
			 	     	   			          
			 	     	   	                    frame.getContentPane().add(Save_btn);
	   	    
            Clear_btn = new JButton("Clear");
	   	    Clear_btn.addActionListener(new ActionListener() {
	   	 	  public void actionPerformed(ActionEvent e) {
	   	 		
	   	      Student_id_tf.setText("");
	   	      
	   	      Coach_id_tf.setText("");
	   	      
	   	      First_name_tf.setText("");
	   	      
	   	      Last_name_tf.setText("");
	   	 
	   	      Class_cb.setSelectedIndex(0);  
	   	      
	   	      date_dc.setDate(null);
	   	      
	   	      Time_cb.setSelectedIndex(0);
	   	      
	   	      Time_tc.getTimeField().setValue("00:00:00");
	   	 
	   	 	}
	   	 });
	   		   	 
	   	    Clear_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/clear.png")));
	   	    
            Clear_btn.setBounds(1009, 139, 124, 33);
	   		   	          
	   		   	      frame.getContentPane().add(Clear_btn);
	   	        

            Exit_btn = new JButton("Exit"); 
            Exit_btn.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent arg0) {
		     int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
		     if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

		              {
		               System.exit(0);
		              }
	}
});
            Exit_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png"))); 
            
            Exit_btn.setBounds(1009, 180, 124, 33);
	   				          
	   		         frame.getContentPane().add(Exit_btn);
	   		         
	   		      Search = new JLabel("Search"); 
 			        
	   		         Search.setBounds(723, 30, 51, 26); 
		          
		                          frame.getContentPane().add(Search);
		
		                          Search_tf = new JTextField(); 
		                          Search_tf.addKeyListener(new KeyAdapter() {
		                          	@Override
		                          	public void keyReleased(KeyEvent e) {
		                          		 DefaultTableModel model = (DefaultTableModel)Class_booking_table.getModel();
		                                 String search = Student_id_tf.getText();
		                                 TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(model);
		                                 Class_booking_table.setRowSorter(tr);
		                                 tr.setRowFilter(RowFilter.regexFilter(search));	
		                          	}
		                          });
		                          Search_tf.setBounds(786, 32, 210, 22);
		        		          
	                                frame.getContentPane().add(Search_tf);
	   		
            //Class_booking_table = new JTable();
	   		
            //Class_booking_table.setBounds(385, 55, 639, 295);
			
                       //frame.getContentPane().add(Class_booking_table);
                       
            //Class_booking_table_sp = new JScrollPane(Class_booking_table);
   	       	        
                                  //Class_booking_table_sp.setBounds(347, 55, 639, 295);
   
                                                         //frame.getContentPane().add(Class_booking_table_sp, BorderLayout.CENTER);
            
                                                         Logo = new JLabel();
                                                         Logo.setIcon(new ImageIcon(this.getClass().getResource("/img/vlc1.PNG")));
                                               	        
                                                         Logo.setBounds(998, 268, 202, 82);
                                                 
                                                                 frame.getContentPane().add(Logo);
	    } 
	      
	    public static void main(String[] args) { 
	        new StudentClassbooking(); 
	       
	        ClassbookingData    Classbooking1 = new ClassbookingData("Student id", "Coach id", "First Name", "Last Name", "Class", "Room", "Date", "Time", "am/pm");
	        ClassbookingData    Classbooking2 = new ClassbookingData("Student id", "Coach id", "First Name", "Last Name", "Class", "Room", "Date", "Time", "am/pm");
	        ClassbookingData    Classbooking3 = new ClassbookingData("Student id", "Coach id", "First Name", "Last Name", "Class", "Room", "Date", "Time", "am/pm");
	        ClassbookingData    Classbooking4 = new ClassbookingData("Student id", "Coach id", "First Name", "Last Name", "Class", "Room", "Date", "Time", "am/pm");
	        ClassbookingData    Classbooking5 = new ClassbookingData("Student id", "Coach id", "First Name", "Last Name", "Class", "Room", "Date", "Time", "am/pm");
	        ClassbookingData    Classbooking6 = new ClassbookingData("Student id", "Coach id", "First Name", "Last Name", "Class", "Room", "Date", "Time", "am/pm");
	        ClassbookingData    Classbooking7 = new ClassbookingData("Student id", "Coach id", "First Name", "Last Name", "Class", "Room", "Date", "Time", "am/pm");
	        ClassbookingData    Classbooking8 = new ClassbookingData("Student id", "Coach id", "First Name", "Last Name", "Class", "Room", "Date", "Time", "am/pm");
	        ClassbookingData    Classbooking9 = new ClassbookingData("Student id", "Coach id", "First Name", "Last Name", "Class", "Room", "Date", "Time", "am/pm");
	        ClassbookingData    Classbooking10 = new ClassbookingData("Student id", "Coach id", "First Name", "Last Name", "Class", "Room", "Date", "Time", "am/pm");
	        
	        
	        ArrayList <ClassbookingData> list =  Singleton.getInstance().getclassbookingdataList();			
		    
		    list.add(Classbooking1);
		    list.add(Classbooking2);
		    list.add(Classbooking3);
		    list.add(Classbooking4);
		    list.add(Classbooking5);
		    list.add(Classbooking6);
		    list.add(Classbooking7);
		    list.add(Classbooking8);
		    list.add(Classbooking9);
		    list.add(Classbooking10);
	        
	} 
}