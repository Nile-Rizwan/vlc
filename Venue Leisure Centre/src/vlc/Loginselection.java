package vlc;

import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.*;


public class Loginselection  {
	
	//variable names for the system
	
	JFrame frame; 
	JLabel Venue_leisure_centre;      
	JButton Admin_login_btn;       
	JButton Coach_login_btn;     
	JButton Student_login_btn; 
	JButton Appointment_btn; 
	JButton Fee_billing_btn; 
	JButton Class_booking_btn;
	JButton Attendence_btn;
	Container c;
	JLabel Logo;
	
      
	Loginselection() 
    { 
		
         
        frame=new JFrame("Venue Leisure Centre"); 
        Image img = Toolkit.getDefaultToolkit().getImage("Resources/img/vlcmin.PNG");
        frame.setIconImage(img);
        c = frame.getContentPane();
		c.setLayout(new BorderLayout());
    	JPanel centerpanel = new JPanel(new GridLayout(3, 3));
    	JPanel northpanel = new JPanel(new FlowLayout());
    	JPanel southpanel = new JPanel(new FlowLayout());
    	JPanel eastpanel = new JPanel(new FlowLayout());
    	JPanel westpanel = new JPanel(new FlowLayout());

        Venue_leisure_centre = new JLabel("Venue Leisure Centre"); 

        Admin_login_btn = new JButton("Admin Login"); 
        Coach_login_btn = new JButton("CoachLogin"); 
        Student_login_btn = new JButton("Student Login"); 
        
        
        Logo = new JLabel();
        Logo.setIcon(new ImageIcon(this.getClass().getResource("/img/vlc.PNG")));
        
       //panel of the border layout

        northpanel.add(Venue_leisure_centre);
        
		centerpanel.add(Admin_login_btn);       
        centerpanel.add(Coach_login_btn);     
        centerpanel.add(Student_login_btn); 
        eastpanel.add(Logo);
        

        c.add(northpanel, BorderLayout.NORTH);
        c.add(centerpanel, BorderLayout.CENTER);
        c.add(southpanel, BorderLayout.SOUTH);
        c.add(eastpanel, BorderLayout.EAST);
        c.add(westpanel, BorderLayout.WEST);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        
        frame.setSize(622, 312); 
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
          
        frame.setVisible(true);
        
        Admin_login_btn.addActionListener(new ActionListener() {
 	       public void actionPerformed(ActionEvent e) {
 	    	  AdminLogin.main(null);
		       frame.dispose();
 	}
 });
        Admin_login_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/admin login.png"))); 
                             
        
        Coach_login_btn.addActionListener(new ActionListener() {
 	      public void actionPerformed(ActionEvent e) {
 	    	 CoachLogin.main(null);
		      frame.dispose();
 	}
 });
        Coach_login_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/coach reg.png"))); 
        
        
        Student_login_btn.addActionListener(new ActionListener() {
 	       public void actionPerformed(ActionEvent e) {
 	    	  StudentLogin.main(null);
		   frame.dispose();
 	}
 });
        Student_login_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/student login.png"))); 
        
        
        
           
    } 
      
    public static void main(String[] args) { 
        new Loginselection(); 
        
    } 
} 