package vlc;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import com.toedter.calendar.JDateChooser;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;

public class Coachregistration {
	
	//Variables names callings
	 JFrame frame; 
	 JMenuBar menubar2;
	 JMenu filemenu;  JMenuItem Exit;
	 JMenu openmenu;  JMenuItem studentregistration_mi;  JMenuItem records_mi;  JMenuItem appointment_mi;  JMenuItem feebilling_mi;  JMenuItem  classbokking_mi; JMenuItem Attendance_mi;
	 JMenu Logoutmenu; JMenuItem Logout_mi;
	 JMenu Helpmenu; JMenuItem Help_mi;
	 JLabel Coach_registration;
	 JLabel  Coach_id;                       JTextField  Coach_id_tf;
	 JLabel First_name;                      JTextField First_name_tf;
	 JLabel Last_name;                       JTextField Last_name_tf;
	 JLabel Father_name;                    JTextField Father_name_tf;
	 JLabel Date_of_birth;                   JDateChooser Date_of_birth_dc; 
	 JLabel  Date_of_hire;                   JDateChooser  Date_of_hire_dc;
	 JLabel Gender;                          JRadioButton Male_rbtn;     JRadioButton Female_rbtn;     JRadioButton Other_rbtn;
	 ButtonGroup Gender_bg;
	 JLabel Blood_group;                     JComboBox Blood_group_cb;
	 JLabel Basic_medical_tranning;          JCheckBox yes_1_bmt;        JCheckBox no_1_bmt;
	 ButtonGroup Basic_medical_tranning_bg;
	 JLabel Icf_certified;                   JCheckBox yes_2_icf;        JCheckBox no_2_icf;
	 ButtonGroup Icf_certified_bg;
	 JLabel Certifications_aquired;          JTextField Certifications_aquired_tf;
	 JLabel Educational_background;          JTextField Educational_background_tf;
	 JLabel Email;                           JTextField Email_tf;
	 JLabel Telephone_no;                    JTextField Telephone_no_tf;
	 JLabel Emergency_contact;               JTextField Emergency_contact_tf;
	 JLabel Street_no;                       JTextField Street_no_tf;
	 JLabel House_no;                        JTextField House_no_tf;
	 JLabel Zip_code;                        JTextField Zip_code_tf;
	 JLabel Coaches_salary;                  JTextField Coaches_salary_tf;
	 JLabel Previous_experience;             JComboBox Previous_experience_cb;
	 JLabel Class_to_teach;                  JComboBox Class_to_teach_cb;
	 JLabel Alloted_room;                    JComboBox Alloted_room_cb;
	 JLabel picture;
	 JLabel Pic_box;
	 JButton Add_file_btn;
	 JFileChooser file;
	 JButton Load_data_btn;
	 JTable Coach_registration_table;
	 JScrollPane Coach_registration_table_sp;
	 JButton Save_btn;
	 JButton Back_btn;
	 JButton Clear_btn;
	 JButton Exit_btn;
	 JLabel Search;
	 JTextField Search_tf;
	 DefaultTableModel model;
	 
	 JLabel Logo;
	 
	 public ImageIcon ResizeImage(String ImagePath){
			ImageIcon MyImage_1 = new ImageIcon (ImagePath);
			Image img = MyImage_1.getImage();
			Image newImg = img.getScaledInstance(Pic_box.getWidth(), Pic_box.getHeight(), Image.SCALE_SMOOTH);
			ImageIcon image = new ImageIcon(newImg);
			return image;
		}
	 
	 Coachregistration() 
	    { 
		 //Frame code
	         
	        frame=new JFrame("Venue Leisure Centre"); 
	        Image img = Toolkit.getDefaultToolkit().getImage("Resources/img/vlcmin.PNG");
	        frame.setIconImage(img);
	        
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
	        
	        frame.setSize(1167, 1003); 
	        
	        frame.setLocationRelativeTo(null);
	          
	        frame.getContentPane().setLayout(null); 
	        
	          
	        frame.setVisible(true);
	        
	        //Menu bar
	        
	        menubar2 = new JMenuBar ();
	        frame.setJMenuBar (menubar2);
	        
	        filemenu = new JMenu ("File");
	        menubar2.add (filemenu);
	        
	       
	        
	        openmenu = new JMenu ("Open");
	        filemenu.add (openmenu);
	        openmenu.setIcon(new ImageIcon(this.getClass().getResource("/icon/open.png")));
	        
	        studentregistration_mi = new JMenuItem("Student Registration");
	        studentregistration_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Studentregistration.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(studentregistration_mi);
	        studentregistration_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/student reg jmi.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        records_mi = new JMenuItem("Records");
	        records_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Report.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(records_mi);
	        records_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/records jmi.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        appointment_mi = new JMenuItem("Appointment");
	        appointment_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Appointment.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(appointment_mi);
	        appointment_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/appointment jmi.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        feebilling_mi = new JMenuItem("Fee Billing");
	        feebilling_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Feebilling.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(feebilling_mi);
	        feebilling_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/fee billing jmi.png")));
	        
	        openmenu.add(new JSeparator());
	        
	        classbokking_mi = new JMenuItem("Class Booking");
	        classbokking_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Classbooking.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(classbokking_mi);
	        classbokking_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/class booking jmi.png")));
	        
 openmenu.add(new JSeparator());
	        
	        Attendance_mi = new JMenuItem("Attendance");
	        Attendance_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        	   StudentAttendance.main(null);
	  		       frame.dispose();
	        	}
	        });
	        openmenu.add(Attendance_mi);
	        Attendance_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/Attendance jmi.png")));
	        
	        
	        filemenu.add(new JSeparator());
	        
	        Exit = new JMenuItem("Exit");
	        Exit.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
	        		 int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
		             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

		                    {
		                     System.exit(0);
		                    }
	        	}
	        });
	        filemenu.add(Exit);
	        Exit.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png")));
	        
	        Logoutmenu = new JMenu ("Logout");
	        menubar2.add (Logoutmenu);
	         
	        
	        Logout_mi = new JMenuItem("Logout");
	        Logout_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent arg0) {
	        		int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to logout?","Logout",JOptionPane.YES_NO_OPTION);
		             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 
		                    {
		            	 Loginselection.main(null);
			                 frame.dispose();
		                    }
	        	}
	        });
	        Logoutmenu.add(Logout_mi);
	        Logout_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/logout.png"))); 
	        
	        Helpmenu = new JMenu ("Help");
	        menubar2.add (Helpmenu);
	         
	        
	        Help_mi = new JMenuItem("Help");
	        Help_mi.addActionListener(new ActionListener() {
	        	public void actionPerformed(ActionEvent e) {
	        		 Help.main(null); 
	        	}
	        });
	        
	        Helpmenu.add(Help_mi);
	        Help_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/help.png")));
	        
	        //label and textfields variable names
	        
	          
	        Coach_registration = new JLabel("Coach Registration"); 
	        
	                          Coach_registration.setBounds(426, 13, 126, 26); 
	          
	                          					frame.getContentPane().add(Coach_registration); 
	        
            Coach_id = new JLabel("Coach ID"); 
	        
                    Coach_id.setBounds(24, 55, 75, 26); 
	        	          
	        	            frame.getContentPane().add( Coach_id);
	        	
	        Coach_id_tf = new JTextField(); 
	        	        
	        	       Coach_id_tf.setBounds(163, 57, 210, 22);
	        	          
	        	                   frame.getContentPane().add( Coach_id_tf);
	        
            First_name = new JLabel("First Name"); 
	        
            		  First_name.setBounds(24, 94, 75, 26); 
	          
            		  			 frame.getContentPane().add(First_name);
	
            First_name_tf = new JTextField(); 
	        
                         First_name_tf.setBounds(163, 98, 210, 22);
	          
	                                   frame.getContentPane().add(First_name_tf);

            Last_name = new JLabel("Last Name"); 
	        
                     Last_name.setBounds(24, 133, 75, 26); 
	          
	                          frame.getContentPane().add(Last_name);
	
            Last_name_tf = new JTextField(); 
	        
                        Last_name_tf.setBounds(163, 137, 210, 22);
	          
	                                 frame.getContentPane().add(Last_name_tf);
	        
            Father_name = new JLabel("Father Name"); 
	        
                        Father_name.setBounds(24, 172, 126, 26); 
	          
	                                frame.getContentPane().add(Father_name);
	
            Father_name_tf = new JTextField(); 
	        
                           Father_name_tf.setBounds(163, 176, 210, 22);
	          
	                                       frame.getContentPane().add(Father_name_tf);
	        
            Date_of_birth = new JLabel("Date Of Birth"); 
	        
            		     Date_of_birth.setBounds(24, 213, 87, 26); 
	          
	                                   frame.getContentPane().add(Date_of_birth);
	        
	        Date_of_birth_dc = new JDateChooser();
	        
	                        Date_of_birth_dc.setBounds(163, 219, 210, 20);
	                        
	                        
	                        
	        	        	        	     frame.getContentPane().add(Date_of_birth_dc);
	        	        	        	      
	        Date_of_hire = new JLabel("Date Of Hire");
	        	        	      	        
	        	        Date_of_hire.setBounds(24, 253, 137, 26);
	        		        	        
	        		                 frame.getContentPane().add( Date_of_hire);
	        		                                              
	        Date_of_hire_dc = new JDateChooser();
	        
	        		       Date_of_hire_dc.setBounds(163, 259, 210, 20);
	        		       
	        		       
	        		       
	        		       	        	   frame.getContentPane().add(Date_of_hire_dc);
	        		                                              
            Gender = new JLabel("Gender"); 
	        
                  Gender.setBounds(24, 292, 51, 26); 
	          
	                    frame.getContentPane().add(Gender);
	
            Male_rbtn = new JRadioButton("Male");
            
            Male_rbtn.setActionCommand("Male");

                     Male_rbtn.setBounds(164, 294, 64, 23);
            
                               frame.getContentPane().add(Male_rbtn);

            Female_rbtn = new JRadioButton("Female");
            
            Female_rbtn.setActionCommand("Female");

                       Female_rbtn.setBounds(232, 294, 75, 23);
             
                                  frame.getContentPane().add(Female_rbtn);
              
            Other_rbtn = new JRadioButton("Other");
            
            Other_rbtn.setActionCommand("Other");

                      Other_rbtn.setBounds(310, 294, 63, 23);
              
                                 frame.getContentPane().add(Other_rbtn);
            
            Gender_bg = new ButtonGroup();
	        Gender_bg.add(Male_rbtn);
	        Gender_bg.add(Female_rbtn);
	        Gender_bg.add(Other_rbtn);
            
            Blood_group = new JLabel("Blood Group");
	        
                       Blood_group.setBounds(24, 331, 97, 26);
            	        	        
                                   frame.getContentPane().add( Blood_group);
            	                    
            Blood_group_cb = new JComboBox();
            	        	        
                          Blood_group_cb.setModel(new DefaultComboBoxModel(new String[] {"O-Positive", "O-Negative", "A-Positive", "A-Negative", "B-Positive", "B-Negative", "AB-Positive", "AB-Negative"}));
            	        	        
                          Blood_group_cb.setBounds(163, 326, 87, 26);
            	        			
                                         frame.getContentPane().add( Blood_group_cb);
            
            Basic_medical_tranning = new JLabel("Basic Medical Tranning"); 
	        
            Basic_medical_tranning.setBounds(25, 370, 138, 26); 
	          
	                               frame.getContentPane().add(Basic_medical_tranning);
	
            yes_1_bmt = new JCheckBox("Yes");
            
            yes_1_bmt.setActionCommand("Yes");

                     yes_1_bmt.setBounds(163, 372, 64, 23);
            
                               frame.getContentPane().add(yes_1_bmt);

            no_1_bmt = new JCheckBox("No");
            
            no_1_bmt.setActionCommand("No");

                    no_1_bmt.setBounds(233, 372, 64, 23);
            
                             frame.getContentPane().add(no_1_bmt);
            
            Basic_medical_tranning_bg = new ButtonGroup();
            Basic_medical_tranning_bg.add(yes_1_bmt);
            Basic_medical_tranning_bg.add(no_1_bmt);
            
            Icf_certified = new JLabel("ICF Certified"); 
	        
            			 Icf_certified.setBounds(24, 409, 138, 26); 
	          
	                                  frame.getContentPane().add(Icf_certified);
	
            yes_2_icf = new JCheckBox("Yes");
            
            yes_2_icf.setActionCommand("Yes");

                     yes_2_icf.setBounds(163, 411, 64, 23);
            
                              frame.getContentPane().add(yes_2_icf);

            no_2_icf = new JCheckBox("No");
            
            no_2_icf.setActionCommand("No");

                    no_2_icf.setBounds(232, 411, 64, 23);
            
                            frame.getContentPane().add(no_2_icf);
            
            Icf_certified_bg = new ButtonGroup();
            Icf_certified_bg.add(yes_2_icf);
            Icf_certified_bg.add(no_2_icf);
            
            Certifications_aquired = new JLabel("Certifications Aquired"); 
	        
                                  Certifications_aquired.setBounds(25, 448, 126, 26); 
	          
	                                                    frame.getContentPane().add(Certifications_aquired);
	
            Certifications_aquired_tf = new JTextField(); 
	        
                                     Certifications_aquired_tf.setBounds(163, 450, 210, 22);
	          
	                                                          frame.getContentPane().add(Certifications_aquired_tf);
	        
            Educational_background = new JLabel("Educational Background"); 
	        
                                  Educational_background.setBounds(385, 55, 146, 26); 
	          
	                                                    frame.getContentPane().add(Educational_background);
	
            Educational_background_tf = new JTextField(); 
	        
                                     Educational_background_tf.setBounds(543, 59, 210, 22);
	          
	                                                           frame.getContentPane().add(Educational_background_tf);
	        
	        Email = new JLabel("Email"); 
	        
	             Email.setBounds(385, 94, 75, 26); 
	        	        	          
	                   frame.getContentPane().add(Email);
	        	        	
	        Email_tf = new JTextField(); 
	        	        	        
	                Email_tf.setBounds(543, 98, 210, 22);
	        	        
	        	            frame.getContentPane().add(Email_tf);
	        	         
            Telephone_no = new JLabel("Telephone NO"); 
	        
                        Telephone_no.setBounds(385, 133, 126, 26); 
	          
	                                 frame.getContentPane().add(Telephone_no);
	
            Telephone_no_tf = new JTextField(); 
	        
                           Telephone_no_tf.setBounds(543, 135, 210, 22);
	          
	                                       frame.getContentPane().add(Telephone_no_tf);
	        
            Emergency_contact = new JLabel("Emergency Contact"); 
	        
                             Emergency_contact.setBounds(385, 172, 126, 26); 
	        	          
	        	                               frame.getContentPane().add(Emergency_contact);
	        	
            Emergency_contact_tf = new JTextField(); 
	        	        
                                Emergency_contact_tf.setBounds(543, 172, 210, 22);
	        	          
	        	                                     frame.getContentPane().add(Emergency_contact_tf);
	        	                                     
            Street_no = new JLabel("Street No"); 
	        
                     Street_no.setBounds(385, 213, 126, 26); 
	          
	                          frame.getContentPane().add(Street_no);
	
            Street_no_tf = new JTextField(); 
	        
                        Street_no_tf.setBounds(543, 213, 210, 22);
	          
	                                 frame.getContentPane().add(Street_no_tf);
	        
            House_no = new JLabel("House NO"); 
	        
                    House_no.setBounds(385, 253, 126, 26); 
	          
	                         frame.getContentPane().add(House_no);
	
            House_no_tf = new JTextField(); 
	        
                       House_no_tf.setBounds(543, 254, 210, 22);
 	          
	                              frame.getContentPane().add(House_no_tf);
	        
            Zip_code = new JLabel("Zip Code"); 
	        
                    Zip_code.setBounds(385, 292, 126, 26); 
	          
	                        frame.getContentPane().add(Zip_code);
	
            Zip_code_tf = new JTextField(); 
	        
                       Zip_code_tf.setBounds(543, 294, 210, 22);
	          
	                              frame.getContentPane().add(Zip_code_tf);
	        
	        Coaches_salary = new JLabel(" Coaches Salary"); 
	        
	                      Coaches_salary.setBounds(385, 331, 110, 26); 
	        	          
	        	                         frame.getContentPane().add(Coaches_salary);
	        	
	        Coaches_salary_tf = new JTextField(); 
	        	        
	        	             Coaches_salary_tf.setBounds(543, 334, 210, 22);
	        	          
	        	                              frame.getContentPane().add(Coaches_salary_tf);
	        
            Previous_experience = new JLabel("Previous Experience");
	        
                               Previous_experience.setBounds(385, 370, 146, 26);
	        
                                                   frame.getContentPane().add(Previous_experience);
            
            Previous_experience_cb = new JComboBox();
	        
	             Previous_experience_cb.setModel(new DefaultComboBoxModel(new String[] {"1 Years", "2 Years", "3 Years", "4 Years", "5 Years", "6 Years", "7 Years"}));
	        
	             Previous_experience_cb.setBounds(553, 371, 87, 26);
			
	                  frame.getContentPane().add(Previous_experience_cb);
	        
            Class_to_teach = new JLabel("Class to Teach");
	        
                          Class_to_teach.setBounds(385, 409, 97, 26);
	        
                                        frame.getContentPane().add(Class_to_teach);
            
            Class_to_teach_cb = new JComboBox();
	        
	              Class_to_teach_cb.setModel(new DefaultComboBoxModel(new String[] {"Swimming", "badminton", "gym"}));
	        
	              Class_to_teach_cb.setBounds(553, 409, 87, 26);
			
	                    frame.getContentPane().add(Class_to_teach_cb);
	        
           Alloted_room = new JLabel("Alloted Room");
	        
                       Alloted_room.setBounds(385, 448, 97, 26);
	        
                                   frame.getContentPane().add(Alloted_room);
            
            Alloted_room_cb = new JComboBox();
	        
	             Alloted_room_cb.setModel(new DefaultComboBoxModel(new String[] {"Studio A", "Studio B", "Studio C", "Pool", "Gym"}));
	        
	             Alloted_room_cb.setBounds(553, 446, 87, 26);
			
	                   frame.getContentPane().add(Alloted_room_cb);
	                   
	        picture = new JLabel("Picture");
	        
                   picture.setBounds(830, 35, 51, 16);
                   
                           frame.getContentPane().add(picture);
                       
            Pic_box = new JLabel("");
            
                   Pic_box.setBounds(768, 55, 171, 193);
                   Border border = BorderFactory.createLineBorder(Color.BLACK, 2);
                   Pic_box.setBorder(border);
                   
                           frame.getContentPane().add(Pic_box);
                       
            Add_file_btn = new JButton("Add Files\r\n");
            Add_file_btn.addActionListener(new ActionListener() {
                       public void actionPerformed(ActionEvent e) {
                    	   
                    	   //Jfile chooser
                       		
            file = new JFileChooser();
                file.setCurrentDirectory(new File(System.getProperty("user.home")));
                FileNameExtensionFilter filter1 = new FileNameExtensionFilter("*.Images", "jpg","gif","png");
                file.addChoosableFileFilter(filter1);
                int result = file.showSaveDialog(null);
                if(result == JFileChooser.APPROVE_OPTION){
                File selectedFile = file.getSelectedFile();
                String path = selectedFile.getAbsolutePath();
                Pic_box.setIcon(ResizeImage(path));
                Pic_box.setText(selectedFile.getAbsolutePath());
                                       			              
                                       			              
                }
                                       			          
                  else if(result == JFileChooser.CANCEL_OPTION){
                  System.out.println("No File Selected");
                }
           }
       });
            Add_file_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/add file.png")));
             Add_file_btn.setBounds(789, 253, 124, 33);
             frame.getContentPane().add(Add_file_btn);
             
             
             //Load data button for array list
             Load_data_btn = new JButton("Load Data"); 
             Load_data_btn.addActionListener(new ActionListener() {
             	public void actionPerformed(ActionEvent e) {
             		 ArrayList<CoachregistrationData> vector = Singleton.getInstance().getcoachregistrationdataList();
	   		            String[] columnNames = {"Coach Id", "First Name", "Last Name", "Fathers Name", "Date of Birth", "Date of Hire", "Gender", "Blood Group", "Basic Medical Tranning", "Icf Certified", "Certifications Aquired", "Educational Background", "Email", "Telephone No", "Emergency Contact", "Street No", "House No", "Zip Code", "Coaches Salary", "Previous Experience", "Class to Teach", "Alloted Room"}; 

	   		  	 Object[][] data;
	   		  	 
				data = new Object[vector.size()][22];

				for (int i = 0; i < data.length; i++) {
					CoachregistrationData a = vector.get(i);
					data[i][0] = a.getCoach_id();
					data[i][1] = a.getFirst_name();
					data[i][2] = a.getLast_name();
					data[i][3] = a.getFather_name();
					data[i][4] = a.getDate_of_birth_dc();
					data[i][5] = a.getDate_of_hire_dc();
					data[i][6] = a.getGender_bg();
					data[i][7] = a.getBlood_group_cb();
					data[i][8] = a.getBasic_medical_tranning_bg();
					data[i][9] = a.getIcf_certified_bg();
					data[i][10] = a.getCertifications_aquired();
					data[i][11] = a.getEducational_background();
					data[i][12] = a.getEmail();
					data[i][13] = a.getTelephone_no();
					data[i][14] = a.getEmergency_contact();
					data[i][15] = a.getStreet_no();
					data[i][16] = a.getHouse_no();
					data[i][17] = a.getZip_code();
					data[i][18] = a.getCoaches_salary();
					data[i][19] = a.getPrevious_experience_cb();
					data[i][20] = a.getClass_to_teach_cb();
					data[i][21] = a.getAlloted_room_cb();
					

					
					
					

	               
					
					
				}
	   				model = new DefaultTableModel(data, columnNames);
	   				Coach_registration_table = new JTable(model);
	   				Coach_registration_table_sp  = new JScrollPane(Coach_registration_table);

	   				Coach_registration_table_sp.setVisible(true);

	   				Coach_registration_table_sp.setBounds(12, 487, 913, 430);

	                     frame.getContentPane().add(Coach_registration_table_sp);	
	                  
	                 }
	                 
            });		
             	
             
             Load_data_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/load data.png"))); 
             
             Load_data_btn.setBounds(951, 55, 124, 33);
 	        	        	                    	   	          
                           frame.getContentPane().add(Load_data_btn);
        //save button for array list
             Save_btn = new JButton("Save"); 
             Save_btn.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) {
		
		 ArrayList<CoachregistrationData> al = Singleton.getInstance().getcoachregistrationdataList();
		 CoachregistrationData fbd = new CoachregistrationData(Coach_id_tf.getText(), First_name_tf.getText(), Last_name_tf.getText(), Father_name_tf.getText(), Date_of_birth_dc.getDate().toString(), Date_of_hire_dc.getDate().toString(), Gender_bg.getSelection().getActionCommand(), Blood_group_cb.getSelectedItem().toString(), Basic_medical_tranning_bg.getSelection().getActionCommand(), Icf_certified_bg.getSelection().getActionCommand(), Certifications_aquired_tf.getText(), Educational_background_tf.getText(), Email_tf.getText(), Telephone_no_tf.getText(), Emergency_contact_tf.getText(), Street_no_tf.getText(), House_no_tf.getText(), Zip_code_tf.getText(), Coaches_salary_tf.getText(), Previous_experience_cb.getSelectedItem().toString(), Class_to_teach_cb.getSelectedItem().toString(), Alloted_room_cb.getSelectedItem().toString());
	 
        	 al.add(fbd);
        	JOptionPane.showMessageDialog(null, "Data has been successfully inserted");    
         }
    });	

Save_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/save.png")));
	        
     Save_btn.setBounds(951, 96, 124, 33);
	          
                frame.getContentPane().add(Save_btn);
                
	        
            Back_btn = new JButton("Back"); 
                    Back_btn.addActionListener(new ActionListener() {
	                  public void actionPerformed(ActionEvent e) {
		              Mainpage.main(null);
		              frame.dispose();
	}
});
                    
                    Back_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/back.png"))); 
	   	        
                    Back_btn.setBounds(951, 135, 124, 33);
	   	          
	   	                    frame.getContentPane().add(Back_btn);
	   	                    
	   	                    //Clear Button
	   	        
            Clear_btn = new JButton("CLear"); 
                     Clear_btn.addActionListener(new ActionListener() {
	                   public void actionPerformed(ActionEvent e) {
		 
		               Coach_id_tf.setText("");
		 
		               First_name_tf.setText("");
		 
		               Last_name_tf.setText("");
		
		               Father_name_tf.setText("");
		 
		               Date_of_birth_dc.setDate(null);
		               
		          	   Date_of_hire_dc.setDate(null);
		          	   
		               Gender_bg.clearSelection();
		 
		               Blood_group_cb.setSelectedIndex(0);
		
		               Basic_medical_tranning_bg.clearSelection();
		 
		               Icf_certified_bg.clearSelection();
		 
		               Certifications_aquired_tf.setText("");
		 
		               Educational_background_tf.setText("");
		 
		               Email_tf.setText("");
		 
		               Telephone_no_tf.setText("");
		 
		               Emergency_contact_tf.setText("");
		
		               Street_no_tf.setText("");
		 
		               House_no_tf.setText("");
		 
		               Zip_code_tf.setText("");
		
		               Coaches_salary_tf.setText("");
		 
		               Previous_experience_cb.setSelectedIndex(0);
		 
		               Class_to_teach_cb.setSelectedIndex(0);
		 
		               Alloted_room_cb.setSelectedIndex(0);
		
		
	}
});
	   		        
                     Clear_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/clear.png")));
                     
                     Clear_btn.setBounds(951, 172, 124, 33);
	   		          
	   		                  frame.getContentPane().add(Clear_btn);
	   		                  
	   		              
//Exit Button
            Exit_btn = new JButton("Exit"); 
                    Exit_btn.addActionListener(new ActionListener() {
	                  public void actionPerformed(ActionEvent arg0) {
		              int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
		              if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

		                   {
		                    System.exit(0);
		                   }
	}
});
                    
           Exit_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png")));          
	   				        
           Exit_btn.setBounds(951, 209, 124, 33);
	   				          
	   		       frame.getContentPane().add(Exit_btn);
	   		       
	   		    Search = new JLabel("Search"); 
			        
  		         Search.setBounds(663, 448, 52, 26); 
	          
	                          frame.getContentPane().add(Search);
	
	                          Search_tf = new JTextField(); 
	                          Search_tf.addKeyListener(new KeyAdapter() {
	                          	@Override
	                          	public void keyReleased(KeyEvent e) {
	                          		 DefaultTableModel model = (DefaultTableModel)Coach_registration_table.getModel();
	                                 String search = Coach_id_tf.getText();
	                                 TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(model);
	                                 Coach_registration_table.setRowSorter(tr);
	                                 tr.setRowFilter(RowFilter.regexFilter(search));	
	                          	}
	                          });
	        
	                          Search_tf.setBounds(716, 450, 210, 22);
	          
	                                frame.getContentPane().add(Search_tf);
	   		
           /*Coach_registration_table = new JTable();
	   		
           Coach_registration_table.setBounds(24, 487, 889, 427);
			
                      frame.getContentPane().add(Coach_registration_table);
                      
           Coach_registration_table_sp = new JScrollPane(Coach_registration_table);
    	       	        
                                      Coach_registration_table_sp.setBounds(24, 487, 889, 427);
  
                                                                  frame.getContentPane().add(Coach_registration_table_sp, BorderLayout.CENTER);*/
			
	   		
                                                                  Logo = new JLabel();
                                                                  Logo.setIcon(new ImageIcon(this.getClass().getResource("/img/vlc1.PNG")));
                                                        	        
                                                                  Logo.setBounds(925, 832, 202, 82);
                                                          
                                                                          frame.getContentPane().add(Logo);
	    } 
	      
	    public static void main(String[] args) { 
	        new Coachregistration(); 
	        //Dummy Data
	        CoachregistrationData    Coachregistration1 = new CoachregistrationData("102", "Hussain", "Bangash", "bangash", "Mon May 23 00:19:50 PKT 1989", "Mon May 07 00:19:50 PKT 2019", "Male",   "O-Ppositive", "yes", "NO",  "", "B-com", "123@gmail.com", "0321478823", "02147895547", "35", "255", "44000", "20000", "i Year", "Swimmimg", "pool");
	        CoachregistrationData    Coachregistration2 = new CoachregistrationData("103", "arooba",  "jamil",   "jamil",   "Mon May 23 00:19:50 PKT 1975", "Mon May 07 00:19:50 PKT 2019", "Female", "O-Ppositive", "yes", "Yes", "", "B-com", "123@gmail.com", "0321478823", "02147895547", "26", "256", "44000", "20000", "2 Years", "Badminton", "Studio B");
	        CoachregistrationData    Coachregistration3 = new CoachregistrationData("104", "sofie",   "metzeker","metzeker","Mon May 23 00:19:50 PKT 1980", "Mon May 07 00:19:50 PKT 2019", "Female", "O-Ppositive", "yes", "No",  "", "HND",   "123@gmail.com", "0321478823", "02147895547", "32", "257", "44000", "20000", "3 Years", "Gym", "Gym");
	        CoachregistrationData    Coachregistration4 = new CoachregistrationData("105", "zaheed",  "khan",    "khan",    "Mon May 23 00:19:50 PKT 1985", "Mon May 07 00:19:50 PKT 2019", "Male",   "O-Ppositive", "yes", "You", "", "PHD",   "123@gmail.com", "0321478823", "02147895547", "44", "258", "44000", "20000", "1 Year", "Badminton", "Studio A");
	        CoachregistrationData    Coachregistration5 = new CoachregistrationData("106", "junaid",  "jumshed", "jumshed", "Mon May 23 00:19:50 PKT 1990", "Mon May 07 00:19:50 PKT 2019", "Male",   "O-Ppositive", "yes", "No",  "", "HND",   "123@gmail.com", "0321478823", "02147895547", "45", "259", "44000", "20000", "1 Year", "Gym", "Gym");
	        
	        
	        ArrayList <CoachregistrationData> list =  Singleton.getInstance().getcoachregistrationdataList();			
		    
		    list.add(Coachregistration1);
		    list.add(Coachregistration2);
		    list.add(Coachregistration3);
		    list.add(Coachregistration4);
		    list.add(Coachregistration5);
	        
	        
	    } 
	} 