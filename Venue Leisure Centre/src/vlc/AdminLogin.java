package vlc;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.util.Map;
import java.awt.event.*; 
import javax.swing.*;




public class AdminLogin implements ActionListener{ 
	
    JFrame frame; 
    JLabel Vlc;
    JLabel Username;    JTextField Username_tf;
    JLabel Password;    JPasswordField Password_pf;
    JButton Login_btn;
    JButton Back_btn;
    Container c;
    JLabel Logo;
    AdminLogin() 
    { 
         
        frame=new JFrame("Venue Leisure Centre"); 
        Image img = Toolkit.getDefaultToolkit().getImage("Resources/img/vlcmin.PNG");
        frame.setIconImage(img);
        c = frame.getContentPane();
		c.setLayout(new BorderLayout());
		//Creation of panels
    	JPanel centerpanel = new JPanel(new GridLayout(4, 3));
    	JPanel northpanel = new JPanel(new FlowLayout());
    	JPanel southpanel = new JPanel(new FlowLayout());
    	JPanel eastpanel = new JPanel(new FlowLayout());
    	JPanel westpanel = new JPanel(new FlowLayout());

    	//Creating of objects
    	Vlc = new JLabel("Venue Leisure Centre (Admin Login)");
    	Username = new JLabel("Username");
    	Username_tf = new JTextField();
    	Password = new JLabel("Password"); 
    	Password_pf = new JPasswordField(); 
    	Login_btn = new JButton("Login"); 
    	Back_btn = new JButton("Back");
    	Back_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/back.png")));
    	Logo = new JLabel();
    	Logo.setIcon(new ImageIcon(this.getClass().getResource("/img/vlc.PNG")));
    	
    	  
    	//Adding panel 
        northpanel.add(Vlc);
		centerpanel.add(Username);       
        centerpanel.add(Username_tf);     
        centerpanel.add(Password); 
        centerpanel.add(Password_pf); 
        southpanel.add( Login_btn); 
        southpanel.add( Back_btn); 
        eastpanel.add(Logo); 
        
        Login_btn.addActionListener(this);
        //adding panels for border layout
        c.add(northpanel, BorderLayout.NORTH);
        c.add(centerpanel, BorderLayout.CENTER);
        c.add(southpanel, BorderLayout.SOUTH);
        c.add(eastpanel, BorderLayout.EAST);
        c.add(westpanel, BorderLayout.WEST);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
        //Code for the frame
        
        frame.setSize(705, 301); 
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);  
          
        frame.setVisible(true);
          
        //Login Button
        Login_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/login.png")));
        
        
        Back_btn.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent arg0) {
    			Loginselection.main(null);
  		      frame.dispose();
    		}
    	});
       
    } 

    public void actionPerformed(ActionEvent e){
    	
    	//array list code
    	
    	if(e.getActionCommand()=="Login" && !Username_tf.getText().isEmpty() && !new String(Password_pf.getPassword()).isEmpty())
    	{
        	Map<String, LoginData> Loginmap = Singleton.getInstance().getAdminUserList();
        	LoginData admin = Loginmap.get(Username_tf.getText());
        	String passText = new String(Password_pf.getPassword());
        	if(admin!=null && passText.equals(admin.getPassword())){
        		frame.dispose();
        		new Mainpage();
        	}  
        	else{
        		JOptionPane.showMessageDialog(null, "Username or Password is incorrect");
        	}
    	}
    	else{
    		JOptionPane.showMessageDialog(null, "Enter username and password feilds cannot be empty");

    	}
    	
    	
    }
    
    
    public static void main(String[] args) { 
    	//array list getting for login page
    	LoginData Login1 = new LoginData ("nile","1122");
    	LoginData Login2 = new LoginData ("naufil","112233");
    	LoginData Login3 = new LoginData ("rafeh","11223344");
    	Map<String, LoginData> Loginmap = Singleton.getInstance().getAdminUserList();
    	
    	Loginmap.put("nile",    Login1);
    	Loginmap.put("naufil",  Login2);
    	Loginmap.put("rafeh",   Login3);
    	
    	
        new AdminLogin(); 
        
    } 
} 