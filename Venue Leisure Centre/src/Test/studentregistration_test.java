package Test;

import static org.junit.Assert.*;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.border.Border;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import org.junit.Test;

import com.toedter.calendar.JDateChooser;

import vlc.Appointment;
import vlc.Classbooking;
import vlc.Coachregistration;
import vlc.Feebilling;
import vlc.Help;
import vlc.Loginselection;
import vlc.Mainpage;
import vlc.Report;
import vlc.Singleton;
import vlc.StudentAttendance;
import vlc.Studentregistration;
import vlc.StudentregistrationData;

public class studentregistration_test {
	JFrame frame;
	 JMenuBar menubar1;
	 JMenu filemenu;  JMenuItem Exit;
	 JMenu openmenu;  JMenuItem coachregistration_mi;  JMenuItem records_mi;  JMenuItem appointment_mi;  JMenuItem feebilling_mi;  JMenuItem  classbokking_mi; JMenuItem Attendance_mi;
	 JMenu Logoutmenu; JMenuItem Logout_mi;
	 JMenu Helpmenu; JMenuItem Help_mi;
	 JLabel Student_registration; 
	 JLabel Student_id;                  JTextField Student_id_tf;         
	 JLabel First_name;                  JTextField First_name_tf;  
	 JLabel Last_name;                   JTextField Last_name_tf;  
	 JLabel Guardians_first_name;        JTextField Guardians_first_name_tf;  	        
	 JLabel Guardians_last_name;         JTextField Guardians_last_name_tf; 
	 JLabel Date_of_birth;               JDateChooser Date_of_birth_dc;
	 JLabel Date_of_admission;           JDateChooser Date_of_admission_dc;
	 JLabel Gender;                      JRadioButton Male_rbtn;             JRadioButton Female_rbtn;     JRadioButton Other_rbtn;
	 ButtonGroup Gender_bg;
	 JLabel Blood_group;                 JComboBox Blood_group_cb;
	 JLabel Email;                       JTextField Email_tf;
	 JLabel Telephone_no;                JTextField Telephone_no_tf; 
	 JLabel Emergency_contact;           JTextField Emergency_contact_tf;
	 JLabel House_no;                    JTextField House_no_tf;       
	 JLabel Street_no;                   JTextField Street_no_tf;  	        
	 JLabel Zip_code;                    JTextField Zip_code_tf;          
	 JLabel Class_to_attend;             JComboBox Class_to_attend_cb;               
	 JLabel Alloted_room;                JComboBox Alloted_room_cb; 	
	 JLabel Picture;
    JLabel Pic_box ;
    JButton Add_file_btn ;
    JFileChooser file;
    JButton Load_data_btn;
    JTable Student_registration_table;   
    JScrollPane Student_registration_table_sp;
    JButton Save_btn; 
	 JButton Back_btn;    	        
	 JButton Clear_btn; 
	 JButton Exit_btn; 
	 JLabel Search;
	 JTextField Search_tf;
	 DefaultTableModel model;
	 
	 
	 JLabel Logo;
	 
	 public ImageIcon ResizeImage(String ImagePath){
			ImageIcon MyImage_1 = new ImageIcon (ImagePath);
			Image img = MyImage_1.getImage();
			Image newImg = img.getScaledInstance(Pic_box.getWidth(), Pic_box.getHeight(), Image.SCALE_SMOOTH);
			ImageIcon image = new ImageIcon(newImg);
			return image;
		}

	@Test
	public void test() {
		
		  
		 
		    { 
		         
		        frame=new JFrame("Venue Leisure Centre");
		        Image img = Toolkit.getDefaultToolkit().getImage("Resources/img/vlcmin.PNG");
		        frame.setIconImage(img);
		        
		        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
		        
		        frame.setSize(1157, 920); 
		        
		        frame.setLocationRelativeTo(null);
		          
		        frame.getContentPane().setLayout(null);   
		          
		        frame.setVisible(true);
		        
		        
		        menubar1 = new JMenuBar ();
		        frame.setJMenuBar (menubar1);
		        
		        filemenu = new JMenu ("File");
		        menubar1.add (filemenu);
		        
		       
		        
		        openmenu = new JMenu ("Open");
		        filemenu.add (openmenu);
		        openmenu.setIcon(new ImageIcon(this.getClass().getResource("/icon/open.png")));
		        
		       
		        
		        coachregistration_mi = new JMenuItem("Coach Registration");
		        coachregistration_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		 Coachregistration.main(null);
		  		       frame.dispose();
		        	}
		        });
		        openmenu.add(coachregistration_mi);
		        coachregistration_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/coach reg jmi.png"))); 
		        
		        openmenu.add(new JSeparator());
		        
		        records_mi = new JMenuItem("Records");
		        records_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		 Report.main(null);
		  		       frame.dispose();
		        	}
		        });
		        openmenu.add(records_mi);
		        records_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/records jmi.png")));
		        
		        openmenu.add(new JSeparator());
		        
		        appointment_mi = new JMenuItem("Appointment");
		        appointment_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		 Appointment.main(null);
		  		       frame.dispose();
		        	}
		        });
		        openmenu.add(appointment_mi);
		        appointment_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/appointment jmi.png")));
		        
		        openmenu.add(new JSeparator());
		        
		        feebilling_mi = new JMenuItem("Fee Billing");
		        feebilling_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		 Feebilling.main(null);
		  		       frame.dispose();
		        	}
		        });
		        openmenu.add(feebilling_mi);
		        feebilling_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/fee billing jmi.png")));
		        
		        openmenu.add(new JSeparator());
		        
		        classbokking_mi = new JMenuItem("Class Booking");
		        classbokking_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		 Classbooking.main(null);
		  		       frame.dispose();
		        	}
		        });
		        openmenu.add(classbokking_mi);
		        classbokking_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/class booking jmi.png")));
		        
	 openmenu.add(new JSeparator());
		        
		        Attendance_mi = new JMenuItem("Attendance");
		        Attendance_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        	   StudentAttendance.main(null);
		  		       frame.dispose();
		        	}
		        });
		        openmenu.add(Attendance_mi);
		        Attendance_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/Attendance jmi.png")));
		        
		        
		        
		        filemenu.add(new JSeparator());
		        
		        Exit = new JMenuItem("Exit");
		        Exit.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent arg0) {
		        		 int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
			             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

			                    {
			                     System.exit(0);
			                    }
		        	}
		        });
		        filemenu.add(Exit);
		        Exit.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png")));
		        
		        Logoutmenu = new JMenu ("Logout");
		        menubar1.add (Logoutmenu);
		         
		        
		        Logout_mi = new JMenuItem("Logout");
		        Logout_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent arg0) {
		        		int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to logout?","Logout",JOptionPane.YES_NO_OPTION);
			             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 
			                    {
			            	 Loginselection.main(null);
				                 frame.dispose();
			                    }
		        	}
		        });
		        Logoutmenu.add(Logout_mi);
		        Logout_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/logout.png"))); 
		        
		        Helpmenu = new JMenu ("Help");
		        menubar1.add (Helpmenu);
		         
		        
		        Help_mi = new JMenuItem("Help");
		        Help_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		 Help.main(null); 
		        	}
		        });
		        
		        Helpmenu.add(Help_mi);
		        Help_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/help.png")));
		        
		          
	            Student_registration = new JLabel("Student Registration"); 
		        
	                                Student_registration.setBounds(426, 13, 126, 26); 
		          
		                                                 frame.getContentPane().add(Student_registration); 
		        
	            Student_id = new JLabel("Student ID"); 
		        
		                  Student_id.setBounds(24, 55, 75, 26); 
		        	          
		        	                 frame.getContentPane().add(Student_id);
		        	
		        Student_id_tf = new JTextField(); 
		        	        
		                     Student_id_tf.setBounds(163, 57, 210, 22);
		        	          
		        	                      frame.getContentPane().add(Student_id_tf);
		        
	            First_name = new JLabel("First Name"); 
		        
	                      First_name.setBounds(24, 94, 75, 26); 
		          
		                            frame.getContentPane().add(First_name);
		
	            First_name_tf = new JTextField(); 
		        
	                         First_name_tf.setBounds(163, 98, 210, 22);
		          
		                                  frame.getContentPane().add(First_name_tf);

	            Last_name = new JLabel("Last Name"); 
		        
	                     Last_name.setBounds(24, 133, 75, 26); 
		          
		                           frame.getContentPane().add(Last_name);
		
	            Last_name_tf = new JTextField(); 
		        
	                        Last_name_tf.setBounds(163, 137, 210, 22);
		          
		                                frame.getContentPane().add(Last_name_tf);
		        
	            Guardians_first_name = new JLabel("Guardian's First Name"); 
		        
	                                Guardians_first_name.setBounds(24, 172, 126, 26); 
		          
		                                                 frame.getContentPane().add(Guardians_first_name);
		
	            Guardians_first_name_tf = new JTextField(); 
		        
	                                   Guardians_first_name_tf.setBounds(163, 176, 210, 22);
		          
		                                                       frame.getContentPane().add(Guardians_first_name_tf);
		        
	            Guardians_last_name = new JLabel("Guardian's Last Name"); 
		        
	                               Guardians_last_name.setBounds(24, 207, 126, 26); 
		          
		                                              frame.getContentPane().add(Guardians_last_name);
		
	            Guardians_last_name_tf = new JTextField(); 
		        
	                                  Guardians_last_name_tf.setBounds(163, 209, 210, 22);
		          
		                                                     frame.getContentPane().add(Guardians_last_name_tf);

	            Date_of_birth = new JLabel("Date Of Birth"); 
		        
	                         Date_of_birth.setBounds(24, 246, 87, 26); 
		          
		                                   frame.getContentPane().add(Date_of_birth);
		        
		        Date_of_birth_dc = new JDateChooser();
		        
		                        Date_of_birth_dc.setBounds(163, 252, 210, 20);
		                        
		                        
		                                   
		        	        	        	     frame.getContentPane().add(Date_of_birth_dc);
		        	        	        	      
		        Date_of_admission = new JLabel("Date Of Admission");
		        	        	      	        
		        	        	 Date_of_admission.setBounds(24, 285, 137, 26);
		        		        	        
		        		                           frame.getContentPane().add( Date_of_admission);
		        		                                              
		        Date_of_admission_dc = new JDateChooser();
		        		                                              
		                            Date_of_admission_dc.setBounds(163, 288, 210, 20);
		        		                                              
		                            
		        		                                              
		        		       	        	        	 frame.getContentPane().add(Date_of_admission_dc);
		        
	            Gender = new JLabel("Gender"); 
		        
	                   Gender.setBounds(24, 324, 51, 26); 
		        	          
		                      frame.getContentPane().add(Gender);
		        	
	            Male_rbtn = new JRadioButton("Male");
	            
	            Male_rbtn.setActionCommand("Male");

	                     Male_rbtn.setBounds(163, 326, 64, 23);
		                    
		                          frame.getContentPane().add(Male_rbtn);

	            Female_rbtn = new JRadioButton("Female");
	            
	            Female_rbtn.setActionCommand("Female");

	                       Female_rbtn.setBounds(231, 326, 75, 23);
		                     
		                               frame.getContentPane().add(Female_rbtn);
		                      
	            Other_rbtn = new JRadioButton("Other");
	            
	            Other_rbtn.setActionCommand("Other");

	                      Other_rbtn.setBounds(310, 326, 63, 23);
		                      
		                            frame.getContentPane().add(Other_rbtn);
		        
		        Gender_bg = new ButtonGroup();
		        Gender_bg.add(Male_rbtn);
		        Gender_bg.add(Female_rbtn);
		        Gender_bg.add(Other_rbtn);
		        
		        
	            Blood_group = new JLabel("Blood Group");
		        
	                       Blood_group.setBounds(24, 363, 97, 26);
		        	        
		                               frame.getContentPane().add( Blood_group);
		                    
	            Blood_group_cb = new JComboBox();
		        	        
	                          Blood_group_cb.setModel(new DefaultComboBoxModel(new String[] {"O-Positive", "O-Negative", "A-Positive", "A-Negative", "B-Positive", "B-Negative", "AB-Positive", "AB-Negative"}));
		        	        
	                          Blood_group_cb.setBounds(163, 363, 87, 26);
		        			
		                                     frame.getContentPane().add( Blood_group_cb);
		        
	            Email = new JLabel("Email"); 
		        
	                 Email.setBounds(385, 55, 75, 26); 
		        	          
	                      frame.getContentPane().add(Email);
		        	
	            Email_tf = new JTextField(); 
		        	        
	                    Email_tf.setBounds(524, 59, 210, 22);
		        
		                         frame.getContentPane().add(Email_tf);
		        	        
	            Telephone_no = new JLabel("Telephone No");
		        	        
	                        Telephone_no.setBounds(385, 94, 87, 26);
		                    	        
		                                frame.getContentPane().add(Telephone_no);
		                                
	            Telephone_no_tf = new JTextField(); 
		                    	        
	                           Telephone_no_tf.setBounds(524, 96, 210, 22);
		                    	          
		                                       frame.getContentPane().add(Telephone_no_tf);
		                    	        
	            Emergency_contact = new JLabel("Emergency Contact");
		        	        	        
	                             Emergency_contact.setBounds(385, 133, 117, 26);
		        	                    	        
	                                               frame.getContentPane().add(Emergency_contact);
		        	                                
	            Emergency_contact_tf = new JTextField(); 
		        	                    	        
	                                Emergency_contact_tf.setBounds(524, 135, 210, 22);
		        	                    	          
		                                                 frame.getContentPane().add(Emergency_contact_tf);
		        
		                                                 Street_no = new JLabel("Street No");
		                                     	        
		                                                 Street_no.setBounds(385, 171, 75, 26);
		                            	        
		                                                           frame.getContentPane().add(Street_no);
		                                        
		                                        Street_no_tf = new JTextField(); 
		                            	        
		                                                    Street_no_tf.setBounds(524, 173, 210, 22);
		                            	          
		                            	                                 frame.getContentPane().add(Street_no_tf);
		                            	        
		                                                 
	            House_no = new JLabel("House No");
		        
	                    House_no.setBounds(385, 210, 75, 26);
		        
	                             frame.getContentPane().add(House_no);
	            
	            House_no_tf = new JTextField(); 
		        
	                       House_no_tf.setBounds(524, 212, 210, 22);
		          
		                              frame.getContentPane().add(House_no_tf);
		        
	            
	            Zip_code = new JLabel("Zip Code");
		        
	                    Zip_code.setBounds(385, 246, 75, 26);
		        
	                            frame.getContentPane().add(Zip_code);
	            
	            Zip_code_tf = new JTextField(); 
		        
	                       Zip_code_tf.setBounds(524, 251, 210, 22);
		          
		                              frame.getContentPane().add(Zip_code_tf);
		        
	            Class_to_attend = new JLabel("Class to Attend");
		        
	                           Class_to_attend.setBounds(385, 288, 97, 26);
		        
	                                           frame.getContentPane().add(Class_to_attend);
	            
	            Class_to_attend_cb = new JComboBox();
		        
	                              Class_to_attend_cb.setModel(new DefaultComboBoxModel(new String[] {"Swimming", "badminton", "gym"}));
		        
	                              Class_to_attend_cb.setBounds(524, 288, 87, 26);
				
		                                            frame.getContentPane().add(Class_to_attend_cb);
		        
	            Alloted_room = new JLabel("Alloted Room");
		        
	                        Alloted_room.setBounds(385, 327, 97, 26);
		        
	                                     frame.getContentPane().add(Alloted_room);
	            
	            Alloted_room_cb = new JComboBox();
		        
	                           Alloted_room_cb.setModel(new DefaultComboBoxModel(new String[] {"Studio A", "Studio B", "Studio C", "Pool", "Gym"}));
		        
	                           Alloted_room_cb.setBounds(524, 326, 87, 28);
				
		                                       frame.getContentPane().add(Alloted_room_cb);
		                                       
		       Picture = new JLabel("Picture");
		       
		              Picture.setBounds(810, 33, 51, 16);
		              
	                          frame.getContentPane().add(Picture);
	                                           
	           Pic_box = new JLabel("");
	           
	                  Pic_box.setBounds(746, 55, 171, 193);
	                  
	                  Border border = BorderFactory.createLineBorder(Color.BLACK, 2);
	                  
	                  Pic_box.setBorder(border);
	                  
	                          frame.getContentPane().add(Pic_box);
	                                           
	           Add_file_btn = new JButton("Add File");
	                   Add_file_btn.addActionListener(new ActionListener() {
	                            public void actionPerformed(ActionEvent e) {
	                                           		
	           file = new JFileChooser();
	               file.setCurrentDirectory(new File(System.getProperty("user.home")));
	                   FileNameExtensionFilter filter1 = new FileNameExtensionFilter("*.Images", "jpg","gif","png");
	                    file.addChoosableFileFilter(filter1);
	                    int result = file.showSaveDialog(null);
	                    if(result == JFileChooser.APPROVE_OPTION){
	                    File selectedFile = file.getSelectedFile();
	                    String path = selectedFile.getAbsolutePath();
	                    Pic_box.setIcon(ResizeImage(path));
	                    Pic_box.setText(selectedFile.getAbsolutePath());
	                         			              
	                         			              
	                    }
	                         			          
	                       else if(result == JFileChooser.CANCEL_OPTION){
	                         	   System.out.println("No File Selected");
	                    }
	              }
	       });
	            Add_file_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/add file.png")));
	            Add_file_btn.setBounds(767, 252, 124, 33);
	                     frame.getContentPane().add(Add_file_btn);
	                     
	                     Load_data_btn = new JButton("Load Data"); 
	                     Load_data_btn.addActionListener(new ActionListener() {
	                     	public void actionPerformed(ActionEvent arg0) {
	                     		
	                     		ArrayList<StudentregistrationData> vector = Singleton.getInstance().getstudentregistrationdataList();
	   		   		            String[] columnNames = {"Student Id", "First Nmae", "Last Name", "Guardians First Name", "Guardians Last Name", "Date of Birth", "Date of Admission", "Gender", "Blood Group", "Email", "Telephone No", "Emergency Contact", "Street No", "House No", "Zip Code", "Class to Attend", "Alloted Room"}; 

	   		   		  	 Object[][] data;
	   		   		  	 
	   					data = new Object[vector.size()][18];

	   					for (int i = 0; i < data.length; i++) {
	   						StudentregistrationData a = vector.get(i);
	   						data[i][0] = a.getStudent_id();
	   						data[i][1] = a.getFname();
	   						data[i][2] = a.getLname();
	   						data[i][3] = a.getGuardians_First();
	   						data[i][4] = a.getGuardians_Last();
	   						data[i][5] = a.getDate_of_birth_dc();
	   						data[i][6] = a.getDate_of_admission_dc();
	   						data[i][7] = a.getGender_bg();
	   						data[i][8] = a.getBlood_group_cb();
	   						data[i][9] = a.getEmail();
	   						data[i][10] = a.getTelephone_no();
	   						data[i][11] = a.getEmergency_contact();
	   						data[i][12] = a.getStreet_no();
	   						data[i][13] = a.getHouse_no();
	   						data[i][14] = a.getZip_code();
	   						data[i][15] = a.getClass_to_attend_cb();
	   						data[i][16] = a.getAlloted_room_cb();
	   						
	   					
	 
	   						
	   						
	   						
	   						
	   					}
	   		   				model = new DefaultTableModel(data, columnNames);
	   		   			Student_registration_table = new JTable(model);
	   		   		Student_registration_table_sp  = new JScrollPane(Student_registration_table);

	   		   	Student_registration_table_sp.setVisible(true);

	   		 Student_registration_table_sp.setBounds(24, 402, 893, 420);

		   	                     frame.getContentPane().add(Student_registration_table_sp);	
	   		                  
	   		                 }
	   		                 
	                   });	
	                     
	                     Load_data_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/load data.png")));    
		                    
	                     Load_data_btn.setBounds(929, 55, 124, 33);
		        	        	        	                    	   	          
		                                  frame.getContentPane().add(Load_data_btn);
		                                  
		                                  Save_btn = new JButton("Save"); 
		                                  Save_btn.addActionListener(new ActionListener() {
		                                  	public void actionPerformed(ActionEvent e) {
		                                  		 ArrayList<StudentregistrationData> al = Singleton.getInstance().getstudentregistrationdataList();
		                                  		StudentregistrationData fbd = new StudentregistrationData(Student_id_tf.getText(), First_name_tf.getText(), Last_name_tf.getText(), Guardians_first_name_tf.getText(), Guardians_last_name_tf.getText(), Date_of_birth_dc.getDate().toString(), Date_of_admission_dc.getDate().toString(), Gender_bg.getSelection().getActionCommand(), Blood_group_cb.getSelectedItem().toString(), Email_tf.getText(), Telephone_no_tf.getText(),Emergency_contact_tf.getText(), Street_no_tf.getText(),House_no_tf.getText(), Zip_code_tf.getText(), Class_to_attend_cb.getSelectedItem().toString(), Alloted_room_cb.getSelectedItem().toString());
		       		                	 
		                                  		if(al.size() <=10){
		                                  			
		                                  			al.add(fbd);
		                                  			
		                                  			JOptionPane.showMessageDialog(null, "Data has been successfully inserted"); 
		                                  			
		                                  		}

		                                  		else{
		                                  			
		                                  			JOptionPane.showMessageDialog(null, "Maximum number of student Reached");
		                                  			
		                                  		}
		                                  		
		     		   		                	 
		     		   		                	   
		     		   		                 }
		     		   	                });	
		                                  
		                                  
		                                  Save_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/save.png")));
		          	   			        
		                                  Save_btn.setBounds(929, 96, 124, 33);
		              	   			          
		              	   	                     frame.getContentPane().add(Save_btn);	
		        
	            Back_btn = new JButton("Back"); 
	            Back_btn.addActionListener(new ActionListener() {
		           public void actionPerformed(ActionEvent e) {
			       Mainpage.main(null);
			       frame.dispose();
		}
	});
		   	        
	            Back_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/back.png"))); 
	            
	            Back_btn.setBounds(929, 135, 124, 33);
		   	       
		   	             frame.getContentPane().add(Back_btn);
		   	        
	            Clear_btn = new JButton("Clear");
	            Clear_btn.addActionListener(new ActionListener() {
		          public void actionPerformed(ActionEvent e) {
			      Student_id_tf.setText("");         
			  
			      First_name_tf.setText("");  
			 
			      Last_name_tf.setText("");  
			  
			      Guardians_first_name_tf.setText("");  	        
			
			      Guardians_last_name_tf.setText(""); 
			
			      Date_of_birth_dc.setDate(null);
			      
			 	  Date_of_admission_dc.setDate(null);
			      
			      Gender_bg.clearSelection();
			
			      Blood_group_cb.setSelectedIndex(0);
			
			      Email_tf.setText("");
			           
			      Telephone_no_tf.setText(""); 
			
			      Emergency_contact_tf.setText("");
			           
			      House_no_tf.setText("");       
			 
			      Street_no_tf.setText("");  	        
			     
			      Zip_code_tf.setText("");          
			           
			      Class_to_attend_cb.setSelectedIndex(0);               
			          
			      Alloted_room_cb.setSelectedIndex(0); 
		}
	});

	              Clear_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/clear.png"))); 
	            
	              Clear_btn.setBounds(929, 172, 124, 33);
		   		          
		   		            frame.getContentPane().add(Clear_btn);
		   		
		   		        
	                   

	            Exit_btn = new JButton("Exit"); 
	                    Exit_btn.addActionListener(new ActionListener() {
		                    public void actionPerformed(ActionEvent arg0) {
			                     int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
			                     if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

			                   {
			                     System.exit(0);
			                   }
		}
	});
	            Exit_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png"))); 
	                    
	            Exit_btn.setBounds(929, 207, 124, 33);
	            
	               
		   				          
		   		         frame.getContentPane().add(Exit_btn);
		   		         
		   		         
		   		      Search = new JLabel("Search"); 
	 			        
		   		         Search.setBounds(644, 363, 56, 26); 
			          
			                          frame.getContentPane().add(Search);
			
			                          Search_tf = new JTextField(); 
			                          Search_tf.addKeyListener(new KeyAdapter() {
			                          	@Override
			                          	public void keyReleased(KeyEvent e) {
			                          		 DefaultTableModel model = (DefaultTableModel)Student_registration_table.getModel();
			                                 String search = Student_id_tf.getText();
			                                 TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(model);
			                                 Student_registration_table.setRowSorter(tr);
			                                 tr.setRowFilter(RowFilter.regexFilter(search));	
			                          	}
			                          });
			        
			                          Search_tf.setBounds(707, 367, 210, 22);
			          
			                                frame.getContentPane().add(Search_tf);
		   		
		   		      //Student_registration_table = new JTable();
		   		
		   		   //Student_registration_table.setBounds(24, 402, 889, 427);
				
	                        //frame.getContentPane().add(Student_registration_table);
	                        
	               //Student_registration_table_sp = new JScrollPane(Student_registration_table);
	    	       	        
	                                            //Student_registration_table_sp.setBounds(24, 402, 893, 427);
	    
	                                                                          //frame.getContentPane().add(Student_registration_table_sp, BorderLayout.CENTER);
	                                                                          
	                                                                          Logo = new JLabel();
	                                                                          Logo.setIcon(new ImageIcon(this.getClass().getResource("/img/vlc1.PNG")));
	                                                              	        
	                                                                          Logo.setBounds(925, 747, 202, 82);
	                                                      	        
	                                                                                  frame.getContentPane().add(Logo);                                                       
				
		    } 
		      
		   
		     
		} 
	}


