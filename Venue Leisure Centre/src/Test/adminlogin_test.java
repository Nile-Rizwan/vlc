package Test;

import static org.junit.Assert.*;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.junit.Test;

import vlc.AdminLogin;
import vlc.LoginData;
import vlc.Loginselection;
import vlc.Mainpage;
import vlc.Singleton;

public class adminlogin_test {
	JFrame frame; 
    JLabel Vlc;
    JLabel Username;    JTextField Username_tf;
    JLabel Password;    JPasswordField Password_pf;
    JButton Login_btn;
    JButton Back_btn;
    Container c;
    JLabel Logo;
	@Test
	public void test() {
		
	   
	    { 
	         
	        frame=new JFrame("Venue Leisure Centre"); 
	        Image img = Toolkit.getDefaultToolkit().getImage("Resources/img/vlcmin.PNG");
	        frame.setIconImage(img);
	        c = frame.getContentPane();
			c.setLayout(new BorderLayout());
			//Creation of panels
	    	JPanel centerpanel = new JPanel(new GridLayout(4, 3));
	    	JPanel northpanel = new JPanel(new FlowLayout());
	    	JPanel southpanel = new JPanel(new FlowLayout());
	    	JPanel eastpanel = new JPanel(new FlowLayout());
	    	JPanel westpanel = new JPanel(new FlowLayout());

	    	//Creating of objects
	    	Vlc = new JLabel("Venue Leisure Centre (Admin Login)");
	    	Username = new JLabel("Username");
	    	Username_tf = new JTextField();
	    	Password = new JLabel("Password"); 
	    	Password_pf = new JPasswordField(); 
	    	Login_btn = new JButton("Login"); 
	    	Back_btn = new JButton("Back");
	    	Back_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/back.png")));
	    	Logo = new JLabel();
	    	Logo.setIcon(new ImageIcon(this.getClass().getResource("/img/vlc.PNG")));
	    	
	    	  
	    	//Adding panel 
	        northpanel.add(Vlc);
			centerpanel.add(Username);       
	        centerpanel.add(Username_tf);     
	        centerpanel.add(Password); 
	        centerpanel.add(Password_pf); 
	        southpanel.add( Login_btn); 
	        southpanel.add( Back_btn); 
	        eastpanel.add(Logo); 
	        
	        
	        //adding panels for border layout
	        c.add(northpanel, BorderLayout.NORTH);
	        c.add(centerpanel, BorderLayout.CENTER);
	        c.add(southpanel, BorderLayout.SOUTH);
	        c.add(eastpanel, BorderLayout.EAST);
	        c.add(westpanel, BorderLayout.WEST);

	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	        //Code for the frame
	        
	        frame.setSize(705, 301); 
	        frame.setResizable(false);
	        frame.setLocationRelativeTo(null);  
	          
	        frame.setVisible(true);
	          
	        //Login Button
	        Login_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/login.png")));
	        
	        
	        Back_btn.addActionListener(new ActionListener() {
	    		public void actionPerformed(ActionEvent arg0) {
	    			Loginselection.main(null);
	  		      frame.dispose();
	    		}
	    	});
	       
	    } 

	   
	        
	    } 
	
	}


