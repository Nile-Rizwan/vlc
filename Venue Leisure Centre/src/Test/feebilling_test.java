package Test;

import static org.junit.Assert.*;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import org.junit.Test;

import com.toedter.calendar.JDateChooser;

import lu.tudor.santec.jtimechooser.JTimeChooser;
import vlc.Appointment;
import vlc.Classbooking;
import vlc.Coachregistration;
import vlc.Feebilling;
import vlc.FeebillingData;
import vlc.Help;
import vlc.Loginselection;
import vlc.Mainpage;
import vlc.Report;
import vlc.Singleton;
import vlc.StudentAttendance;
import vlc.Studentregistration;

public class feebilling_test {
	JFrame frame; 
	 JMenuBar menubar5;
	 JMenu filemenu; JMenuItem Exit;
	 JMenu openmenu; JMenuItem studentregistration_mi;  JMenuItem coachregistration_mi;  JMenuItem records_mi;  JMenuItem appointment_mi;  JMenuItem  classbokking_mi; JMenuItem Attendance_mi;
	 JMenu Logoutmenu; JMenuItem Logout_mi;
	 JMenu Helpmenu; JMenuItem Help_mi;
	 JLabel Fee_billing;          
	 JLabel Student_id;                       JTextField Student_id_tf;  
	 JLabel First_name;                       JTextField First_name_tf;  
	 JLabel Last_name;                        JTextField Last_name_tf;  
	 JLabel Payment_type;                     JRadioButton  Debit_or_credit_card_rbtn; JRadioButton  Checking_account_rbtn; JRadioButton  Cash_rbtn; JRadioButton  Cheque_rbtn;
	 ButtonGroup Payment_type_bg;
	 JLabel Card_number;                      JTextField Card_number_tf;
	 JLabel Cvv_code;                         JTextField Cvv_code_tf;
	 JLabel Credit_card_type;                 JComboBox Credit_card_type_cb;
	 JLabel Valid_from;                       JDateChooser Valid_from_dc;
	 JLabel Valid_to;                         JDateChooser Valid_to_dc;
	 JLabel Payment_date;                     JDateChooser Payment_date_dc;
	 JLabel Payment_time;                     JTimeChooser Payment_time_tc; JComboBox Payment_time_cb;
	 JLabel Fees_paid;                        JTextField Fees_paid_tf;   
	 JLabel Rs1;
	 JLabel Fees_due;                         JTextField Fees_due_tf;
	 JLabel Rs2;
	 JButton Load_data_btn;
	 JTable Fee_billing_table;
	 JScrollPane Fee_billing_table_sp;
	 JButton Save_btn;
	 JButton Back_btn;    	        
	 JButton Clear_btn;    		        
	  
	 JButton Exit_btn; 
	 JLabel Search;
	 JTextField Search_tf;
	
	 JLabel Logo;
	 DefaultTableModel model;
	@Test
	public void test() {
		
		 
		 
		  
		  
		    { 
		         
		        frame=new JFrame("Venue Leisure Centre"); 
		        Image img = Toolkit.getDefaultToolkit().getImage("Resources/img/vlcmin.PNG");
		        frame.setIconImage(img);
		        
		        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
		        
		        frame.setSize(1339, 734); 
		        
		        frame.setLocationRelativeTo(null);
		          
		        frame.getContentPane().setLayout(null);   
		          
		        frame.setVisible(true);
		        
		        menubar5 = new JMenuBar ();
		        frame.setJMenuBar (menubar5);
		        
		        filemenu = new JMenu ("File");
		        menubar5.add (filemenu);
		        
		       //menu bar
		        
		        openmenu = new JMenu ("Open");
		        filemenu.add (openmenu);
		        openmenu.setIcon(new ImageIcon(this.getClass().getResource("/icon/open.png")));
		        
		        studentregistration_mi = new JMenuItem("Student Registration");
		        studentregistration_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		 Studentregistration.main(null);
		  		       frame.dispose();
		        	}
		        });
		        openmenu.add(studentregistration_mi);
		        studentregistration_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/student reg jmi.png")));
		        
		        openmenu.add(new JSeparator());
		        
		        coachregistration_mi = new JMenuItem("Coach Registration");
		        coachregistration_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		 Coachregistration.main(null);
		  		       frame.dispose();
		        	}
		        });
		        openmenu.add(coachregistration_mi);
		        coachregistration_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/coach reg jmi.png"))); 
		        
		        openmenu.add(new JSeparator());
		        
		        records_mi = new JMenuItem("Records");
		        records_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		 Report.main(null);
		  		       frame.dispose();
		        	}
		        });
		        openmenu.add(records_mi);
		        records_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/records jmi.png")));
		        
		        openmenu.add(new JSeparator());
		        
		        appointment_mi = new JMenuItem("Appointment");
		        appointment_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		 Appointment.main(null);
		  		       frame.dispose();
		        	}
		        });
		        openmenu.add(appointment_mi);
		        appointment_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/appointment jmi.png")));
		        
		        openmenu.add(new JSeparator());
		        
		        classbokking_mi = new JMenuItem("Class Booking");
		        classbokking_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		 Classbooking.main(null);
		  		       frame.dispose();
		        	}
		        });
		        openmenu.add(classbokking_mi);
		        classbokking_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/class booking jmi.png")));
		        
	 openmenu.add(new JSeparator());
		        
		        Attendance_mi = new JMenuItem("Attendance");
		        Attendance_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        	   StudentAttendance.main(null);
		  		       frame.dispose();
		        	}
		        });
		        openmenu.add(Attendance_mi);
		        Attendance_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/Attendance jmi.png")));
		        
		        
		        
		        
		        filemenu.add(new JSeparator());
		        
		        Exit = new JMenuItem("Exit");
		        Exit.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent arg0) {
		        		 int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
			             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

			                    {
			                     System.exit(0);
			                    }
		        	}
		        });
		        filemenu.add(Exit);
		        Exit.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png")));
		        
		        Logoutmenu = new JMenu ("Logout");
		        menubar5.add (Logoutmenu);
		         
		        
		        Logout_mi = new JMenuItem("Logout");
		        Logout_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent arg0) {
		        		int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to logout?","Logout",JOptionPane.YES_NO_OPTION);
			             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 
			                    {
			            	 Loginselection.main(null);
				                 frame.dispose();
			                    }
		        	}
		        });
		        Logoutmenu.add(Logout_mi);
		        Logout_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/logout.png"))); 
		        
		        Helpmenu = new JMenu ("Help");
		        menubar5.add (Helpmenu);
		         
		        
		        Help_mi = new JMenuItem("Help");
		        Help_mi.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		 Help.main(null); 
		        	}
		        });
		        
		        Helpmenu.add(Help_mi);
		        Help_mi.setIcon(new ImageIcon(this.getClass().getResource("/icon/help.png")));
		        
		          
		        
		        //Labels and textfeilds
	            Fee_billing = new JLabel("Fee Billing"); 
		        
	                       Fee_billing.setBounds(481, 13, 126, 26); 
		          
		                               frame.getContentPane().add(Fee_billing); 

	            Student_id = new JLabel("Student ID"); 
		        
	                      Student_id.setBounds(12, 49, 75, 26); 
		          
		                            frame.getContentPane().add(Student_id);
		
		        Student_id_tf = new JTextField(); 
		        
		                     Student_id_tf.setBounds(122, 51, 210, 22);
		          
		                                  frame.getContentPane().add(Student_id_tf);
		                                  
		                                  First_name = new JLabel("First Name"); 
		                      	        
		                                  First_name.setBounds(12, 94, 75, 26); 
		            	          
		            	                            frame.getContentPane().add(First_name);
		            	
		                        First_name_tf = new JTextField(); 
		            	        
		                                     First_name_tf.setBounds(122, 96, 210, 22);
		            	          
		            	                                   frame.getContentPane().add(First_name_tf);

		                        Last_name = new JLabel("Last Name"); 
		            	        
		                                 Last_name.setBounds(12, 136, 75, 26); 
		            	          
		            	                          frame.getContentPane().add(Last_name);
		            	
		                        Last_name_tf = new JTextField(); 
		            	        
		                                    Last_name_tf.setBounds(122, 138, 210, 22);
		            	          
		            	                                frame.getContentPane().add(Last_name_tf);

		        Payment_type = new JLabel("Payment Type"); 
		        
		                    Payment_type.setBounds(12, 182, 87, 26); 
		        	          
		        	                    frame.getContentPane().add(Payment_type);
		        	
		        Debit_or_credit_card_rbtn = new JRadioButton(" Debit or Credit Card");
		        
		        Debit_or_credit_card_rbtn.setActionCommand("Debit or Credit Card");

		                                 Debit_or_credit_card_rbtn.setBounds(122, 186, 164, 23);
		                    
		                                                          frame.getContentPane().add( Debit_or_credit_card_rbtn);

		        Checking_account_rbtn = new JRadioButton("Checking Account");
		        
		        Checking_account_rbtn.setActionCommand("Checking Account");

		                             Checking_account_rbtn.setBounds(122, 214, 141, 23);
		                     
		                                                  frame.getContentPane().add(Checking_account_rbtn);
		                      
		        Cash_rbtn = new JRadioButton("Cash");
		        
		        Cash_rbtn.setActionCommand("Cash");
		       
		                 Cash_rbtn.setBounds(122, 239, 126, 23);
		                      
		                          frame.getContentPane().add(Cash_rbtn);
		        
		        Cheque_rbtn = new JRadioButton("Cheque");
		        
		        Cheque_rbtn.setActionCommand("Cheque");

		                   Cheque_rbtn.setBounds(122, 267, 126, 23);
		                      
		                              frame.getContentPane().add( Cheque_rbtn);
		                    
		                    Payment_type_bg = new ButtonGroup();
		                    Payment_type_bg.add(Debit_or_credit_card_rbtn);
		                    Payment_type_bg.add(Checking_account_rbtn);
		                    Payment_type_bg.add(Cash_rbtn);
		                    Payment_type_bg.add(Cheque_rbtn);
		        
		         Card_number = new JLabel("Card Number"); 
		        	        
		                    Card_number.setBounds(12, 306, 102, 26); 
		                    	          
		                                frame.getContentPane().add(Card_number);
		                    	
		         Card_number_tf = new JTextField(); 
		                    	        
		                       Card_number_tf.setBounds(122, 308, 210, 22);
		                    	          
		                                      frame.getContentPane().add(Card_number_tf);
		        
		         Cvv_code = new JLabel("CVV Code"); 
		        	        
		                 Cvv_code.setBounds(12, 345, 75, 26); 
		       	                    	          
		       	                  frame.getContentPane().add(Cvv_code);
		       	                    	
		       	 Cvv_code_tf = new JTextField(); 
		       	                    	        
		       	            Cvv_code_tf.setBounds(122, 349, 210, 22);
		       	                    	          
		       	                        frame.getContentPane().add(Cvv_code_tf);
		        
		       	 Credit_card_type = new JLabel("Credit Card Type");
		 	        	        
		       	                 Credit_card_type.setBounds(12, 384, 97, 26);
		 	        	        
		 	        	                          frame.getContentPane().add(Credit_card_type);
		 	        	        	        	        
		 	     Credit_card_type_cb = new JComboBox();
		 	        	        
		 	        	            Credit_card_type_cb.setModel(new DefaultComboBoxModel(new String[] {"American Express", "Master Card", "Discover", "Visa", "Jcb"}));
		 	        	        	        
		 	        	            Credit_card_type_cb.setBounds(122, 384, 141, 26);
		 	        	        			
		 	        	        	                   frame.getContentPane().add(Credit_card_type_cb);
		        
		 	     Valid_from = new JLabel("Valid From"); 
		 	        	      	        
		 	        	   Valid_from.setBounds(12, 423, 87, 26); 
		 	        	        	      	          
		 	        	        	 frame.getContentPane().add(Valid_from);
		 	        	        	      	        
		 	     Valid_from_dc = new JDateChooser();
		 	     
		 	        	      Valid_from_dc.setBounds(122, 429, 210, 20);
		 	        	      
		 	        	      
		 	        	      
	  	        	        	         frame.getContentPane().add(Valid_from_dc);
		 	        	        	      	        
		 	     Valid_to = new JLabel("Valid To"); 
		 		 	        	      	        
		 		 	     Valid_to.setBounds(12, 465, 87, 26); 
		 		 	        	        	      	          
		 		 	        	  frame.getContentPane().add(Valid_to);
		 		 	        	        	      	        
		 		 Valid_to_dc = new JDateChooser();
		 		 
		 		 	        Valid_to_dc.setBounds(122, 471, 210, 20);
		 		 	        
		 		 	        
		 		 	        
		        	        	        frame.getContentPane().add(Valid_to_dc);
		        	        	        
		        	        	        Payment_date = new JLabel("Payment Date"); 
			 	        	      	        
		        	        	        Payment_date.setBounds(12, 504, 87, 26); 
		        		 		 	        	        	      	          
		        		 		 	        	  frame.getContentPane().add(Payment_date);
		        		 		 	        	        	      	        
		        		 		 	        	Payment_date_dc = new JDateChooser();
		        		 		 
		        		 		 	        	Payment_date_dc.setBounds(122, 510, 210, 20);
		        		 		 	        
		        		        	        	        frame.getContentPane().add(Payment_date_dc);
		        		        	        	        
		        		        	        	        Payment_time = new JLabel("Payment Time");
		        		        	        	        
		        		                                Payment_time.setBounds(12, 548, 102, 26);
		        		 	        	        
		        		 	                                              frame.getContentPane().add( Payment_time);
		        		        	        	        
		        		 	                                             Payment_time_tc = new JTimeChooser();
		        		        	        	        
		        		 	                                            Payment_time_tc.setBounds(122, 548, 126, 26);
		        			                              
		        		      	        	        	        		         frame.getContentPane().add(Payment_time_tc);
		        		      	        	        	        		         
		        		      	    Payment_time_cb = new JComboBox();
		        		   	        	        	        	        
		        		      	        	        	  Payment_time_cb.setModel(new DefaultComboBoxModel(new String[] {"am", "pm"}));
		        		   	        	        	        	        
		        		      	        	        	  Payment_time_cb.setBounds(268, 548, 64, 26);
		        		   	        	        	        			
		        		   	        	        	        	                 frame.getContentPane().add(Payment_time_cb);
		        
		 		 Fees_paid = new JLabel("Fees Paid"); 
		 		 	        		        	        
		 		 	      Fees_paid.setBounds(12, 585, 75, 26); 
		 		 	        	        		                    	          
		 		 	        	   frame.getContentPane().add(Fees_paid);
		 		 	        	        		                    	
		 		 Fees_paid_tf = new JTextField(); 
		 		 	        	        		                    	        
		 		 	         Fees_paid_tf.setBounds(122, 587, 210, 22);
		 		 	        	        		                    	          
		 		 	        	          frame.getContentPane().add(Fees_paid_tf);
		        
		 		 Rs1 = new JLabel("/RS"); 
		 			 		 	        		        	        
		 		 	Rs1.setBounds(344, 594, 33, 26); 
		 		 	 		 	        	        		                    	          
		 		 	 	frame.getContentPane().add(Rs1);
		        
		 		 Fees_due = new JLabel("Fees Due"); 
		 		 	 		 		 	        		        	        
		 		 	 	 Fees_due.setBounds(12, 624, 75, 26); 
		 		 	 			 		 	        	        		                    	          
		 		 	 			 frame.getContentPane().add(Fees_due);
		 		 	 			 		 	        	        		                    	
		 		 Fees_due_tf = new JTextField(); 
		 		 	 			 		 	        	        		                    	        
		 		 	        Fees_due_tf.setBounds(122, 626, 210, 22);
		 		 	 			 		 	        	        		                    	          
		 		 	 			 		frame.getContentPane().add(Fees_due_tf);
		 		 	 			        
		 		  Rs2 = new JLabel("/RS"); 
		 		 	 			 			 		 	        		        	        
		 		 	 Rs2.setBounds(344, 624, 33, 26); 
		 		 	 			 		 	 		 	        	        		                    	          
		 		 	 	 frame.getContentPane().add(Rs2);
		 		 	 	 //Load data for array list
		 		 	   
		 	             Load_data_btn = new JButton("Load Data"); 
		 	             Load_data_btn.addActionListener(new ActionListener() {
		 	             	public void actionPerformed(ActionEvent arg0) {
		 	             		 ArrayList<FeebillingData> vector = Singleton.getInstance().getfeebillingdataList();
		   		   		            String[] columnNames = {"Student Id", "First Nmae", "Last Name", "Payment Type", "Card Number", "Cvv","Credit card Type", "Valid From", "Valid To", "Payment Date", "Payment Time", "am/pm", "Fees Paid(Rs)", "fees Due(Rs)"}; 

		   		   		  	 Object[][] data;
		   		   		  	 
		   					data = new Object[vector.size()][14];

		   					for (int i = 0; i < data.length; i++) {
		   						FeebillingData a = vector.get(i);
		   						data[i][0] = a.getStudent_id();
		   						data[i][1] = a.getFname();
		   						data[i][2] = a.getLname();
		   						data[i][3] = a.getPayment_bg();
		   						data[i][4] = a.getCardnum();
		   						data[i][5] = a.getCvv();
		   						data[i][6] = a.getCreditcard_cb();
		   						data[i][7] = a.getValidfrom_dc();
		   						data[i][8] = a.getValidto_dc();
		   						data[i][9] = a.getPaymentdate_dc();
		   						data[i][10] = a.getPaymenttime_tc();
		   						data[i][11] = a.getPaymenttime_cb();
		   						data[i][12] = a.getFeespaid();
		   						data[i][13] = a.getFeesdue();
		   						
		   						
		   						
		   						

		 		               
		   						
		   						
		   					}
		   		   				model = new DefaultTableModel(data, columnNames);
		   		   			Fee_billing_table = new JTable(model);
		   		   		    Fee_billing_table_sp  = new JScrollPane(Fee_billing_table);

		   		   	        Fee_billing_table_sp.setVisible(true);

		   		            Fee_billing_table_sp.setBounds(372, 49, 718, 596);

			   	                     frame.getContentPane().add(Fee_billing_table_sp);	
		   		                  
		   		                 }
		   		                 
	                       });	
		 	             	
		 	             
		 	            Load_data_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/load data.png")));
		 	             
		 	             Load_data_btn.setBounds(1102, 49, 124, 33);
		 	 	        	        	                    	   	          
		 	                           frame.getContentPane().add(Load_data_btn);
		 	                           
		 	                           //save button for array list

		 	                          Save_btn = new JButton("Save"); 
		 	                          Save_btn.addActionListener(new ActionListener() {
		 	                          	public void actionPerformed(ActionEvent e) {
		 	                          	 ArrayList<FeebillingData> al = Singleton.getInstance().getfeebillingdataList();
		 	                          	FeebillingData fbd = new FeebillingData(Student_id_tf.getText(), First_name_tf.getText(), Last_name_tf.getText(), Payment_type_bg.getSelection().getActionCommand(),Card_number_tf.getText(),  Cvv_code_tf.getText(), Credit_card_type_cb.getSelectedItem().toString(), Valid_from_dc.getDate().toString(), Valid_to_dc.getDate().toString(), Payment_date_dc.getDate().toString(), Payment_time_tc.getTimeField().getText(), Payment_time_cb.getSelectedItem().toString(), Fees_paid_tf.getText(), Fees_due_tf.getText());
	  		                	 
			   		                	 al.add(fbd);
			   		                	JOptionPane.showMessageDialog(null, "Data has been successfully inserted");    
			   		                 }
			   	                });	
		 	                          	
		 	                          
		 	                         Save_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/save.png")));
		 	 	   			        
		 	                         Save_btn.setBounds(1102, 86, 124, 33);
		 	     	   			          
		 	     	   	                    frame.getContentPane().add(Save_btn);	    
		 	        
		        
	              Back_btn = new JButton("Back"); 
	                      Back_btn.addActionListener(new ActionListener() {
		                    public void actionPerformed(ActionEvent arg0) {
			                Mainpage.main(null);
			                frame.dispose();
		}
	});
	                      Back_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/back.png")));
	                      
	                      Back_btn.setBounds(1102, 125, 124, 33);
		   	          
		   	                       frame.getContentPane().add(Back_btn);
		   	                       
		   	                       //clear button
		   	        
	            Clear_btn = new JButton("CLear"); 
	                     Clear_btn.addActionListener(new ActionListener() {
		                   public void actionPerformed(ActionEvent e) {
			  
	                       Student_id_tf.setText("");   
	                       
	                       First_name_tf.setText(""); 
	                       
	                       Last_name_tf.setText(""); 
			 
			               Payment_type_bg.clearSelection();
			   
			               Card_number_tf.setText(""); 
			 
			               Cvv_code_tf.setText(""); 
			 
			               Credit_card_type_cb.setSelectedIndex(0);
			 
			               Valid_from_dc.setDate(null);
			 
			               Valid_to_dc.setDate(null);
			               
			               Payment_date_dc.setDate(null);
			               
			               Payment_time_tc.getTimeField().setValue("00:00:00");
	       	        	        		         
	                       Payment_time_cb.setSelectedIndex(0); 
			
			               Fees_paid_tf.setText(""); 
			 
			               Fees_due_tf.setText(""); 
			 
		}
	});
		   		        
	            Clear_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/clear.png")));         
	                     
	            Clear_btn.setBounds(1102, 164, 124, 33);
		   		          
		   		          frame.getContentPane().add(Clear_btn);
		   		         
	               
	//Exit Button
	            Exit_btn = new JButton("Exit"); 
	                    Exit_btn.addActionListener(new ActionListener() {
		                 public void actionPerformed(ActionEvent e) {
			             int result = JOptionPane.showConfirmDialog(frame, "Are you sure you want to Exit the system?","Exit System",JOptionPane.YES_NO_OPTION);
			             if(result == JOptionPane.YES_OPTION)//Return value from class method if YES is chosen 

			                      {
			                       System.exit(0);
			                      }
		}
	});
		   		
	            Exit_btn.setIcon(new ImageIcon(this.getClass().getResource("/icon/exit.png")));        
	                    
	            Exit_btn.setBounds(1102, 203, 124, 33);
		   				          
		   		         frame.getContentPane().add(Exit_btn);
		   		         
		   		         
		   		      Search = new JLabel("Search"); 
	 			        
		   		         Search.setBounds(807, 23, 64, 26); 
			          
			                          frame.getContentPane().add(Search);
			
			                          Search_tf = new JTextField(); 
			                          Search_tf.addKeyListener(new KeyAdapter() {
			                          	@Override
			                          	public void keyReleased(KeyEvent e) {
			                          		 DefaultTableModel model = (DefaultTableModel)Fee_billing_table.getModel();
			                                 String search = Student_id_tf.getText();
			                                 TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(model);
			                                 Fee_billing_table.setRowSorter(tr);
			                                 tr.setRowFilter(RowFilter.regexFilter(search));	
			                          	}
			                          });
			        
			                          Search_tf.setBounds(881, 25, 210, 22);
			          
			                                frame.getContentPane().add(Search_tf);
		   		
	            /*Fee_billing_table = new JTable();
		   		
	            Fee_billing_table.setBounds(383, 55, 541, 413);
				
	                       frame.getContentPane().add(Fee_billing_table);
	                       
	            Fee_billing_table_sp = new JScrollPane(Fee_billing_table);
	   	       	        
	                                Fee_billing_table_sp.setBounds(383, 55, 541, 413);
	   
	                                                     frame.getContentPane().add(Fee_billing_table_sp, BorderLayout.CENTER);*/
	                                                     //Logo
	                                                     Logo = new JLabel();
	                                                     Logo.setIcon(new ImageIcon(this.getClass().getResource("/img/vlc1.PNG")));
	                                           	        
	                                                     Logo.setBounds(1092, 568, 217, 82);
	                                             
	                                                             frame.getContentPane().add(Logo);
	                                                             
	                                                             
	                                                             
	                                                           
				
		    } 
		      
		     
		        //Dummy Data
		        
		        FeebillingData    Feebilling1 = new FeebillingData("102589", "First Nmae", "Last Name", "Payment Type", "Card Number", "Cvv","Credit card Type", "Valid From", "Valid To", "Payment Date", "Payment Time", "am/pm", "Fees Paid(Rs)", "fees Due(Rs)");
		        FeebillingData    Feebilling2 = new FeebillingData("102595", "First Nmae", "Last Name", "Payment Type", "Card Number", "Cvv","Credit card Type", "Valid From", "Valid To", "Payment Date", "Payment Time", "am/pm", "Fees Paid(Rs)", "fees Due(Rs)");
		        FeebillingData    Feebilling3 = new FeebillingData("102596", "First Nmae", "Last Name", "Payment Type", "Card Number", "Cvv","Credit card Type", "Valid From", "Valid To", "Payment Date", "Payment Time", "am/pm", "Fees Paid(Rs)", "fees Due(Rs)");
		        FeebillingData    Feebilling4 = new FeebillingData("102597", "First Nmae", "Last Name", "Payment Type", "Card Number", "Cvv","Credit card Type", "Valid From", "Valid To", "Payment Date", "Payment Time", "am/pm", "Fees Paid(Rs)", "fees Due(Rs)");
		        FeebillingData    Feebilling5 = new FeebillingData("102598", "First Nmae", "Last Name", "Payment Type", "Card Number", "Cvv","Credit card Type", "Valid From", "Valid To", "Payment Date", "Payment Time", "am/pm", "Fees Paid(Rs)", "fees Due(Rs)");
		        
		        
		        ArrayList <FeebillingData> list =  Singleton.getInstance().getfeebillingdataList();			
			    
			    list.add(Feebilling1);
			    list.add(Feebilling2);
			    list.add(Feebilling3);
			    list.add(Feebilling4);
			    list.add(Feebilling5);
		        
		    } 
		 
	}


